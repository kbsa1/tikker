﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Tikker.Domain.Models;

namespace Tikker.WPF.Converters
{
    public class BadgeBackgroundConverter : IValueConverter
    {
        /// <summary>
        /// Checks if value is a badge in UserBadges table. Does not check user id for now.
        /// If badge exists in UserBadges, return PrimaryBrush. Otherwise, return BackgroundDarkBrush.
        /// </summary>
        /// <param name="value">Badge</param>
        /// <param name="targetType">Brush</param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //TODO: Does not account for user id yet!
            Badge badge = value as Badge;
            
            // If UserBadges not empty
            if (badge?.UserBadges != null && badge.UserBadges.Count != 0)
            {
                return Application.Current.Resources["PrimaryBrush"];
            }
            return Application.Current.Resources["BackgroundDarkBrush"];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Tikker.Domain.Models;
using Tikker.Domain.Services;
using Tikker.EntityFramework;
using Tikker.EntityFramework.Services;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Commands
{
    /// <summary>
    /// command used to go to the next exercises async.
    /// </summary>
    public class NextExerciseCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        /// <summary>
        /// exercise viewmodel to change the exercise of
        /// </summary>
        private ExerciseViewModel _viewModel;
        /// <summary>
        /// exercise service to get the new exercise from
        /// </summary>
        private IExerciseService _exerciseService;
        /// <summary>
        /// statistics service to get the current user performance from
        /// </summary>
        private IStatisticsService _statisticsService;
        /// <summary>
        /// list of exercises the user is performing
        /// </summary>
        private Queue<Exercise> _exercises = new Queue<Exercise>();

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="exerciseViewModel"></param>
        /// <param name="exerciseService"></param>
        /// <param name="statisticsService"></param>
        public NextExerciseCommand(ExerciseViewModel exerciseViewModel, IExerciseService exerciseService, IStatisticsService statisticsService)
        {
            _viewModel = exerciseViewModel;
            _exerciseService = exerciseService;
            _statisticsService = statisticsService;
        }

        public bool CanExecute(object parameter)
        {
            return _exerciseService != null && _statisticsService != null && _viewModel != null;
        }

        /// <summary>
        /// set next exercise
        /// </summary>
        /// <param name="parameter"></param>
        public async void Execute(object parameter)
        {
            // re-get list of exercises if current one is empty
            if (_exercises.Count < 1)
            {
                // get how good the user is
                DifficultyCategory difficultyCategory = await _statisticsService.GetDifficultyCategory();
                int min;
                int max;
                // beginner
                if (difficultyCategory == DifficultyCategory.Beginner)
                {
                    min = 0;
                    max = 600;
                }
                // average
                else if (difficultyCategory == DifficultyCategory.Average)
                {
                    min = 600;
                    max = 1200;
                }
                // experienced
                else
                {
                    min = 1200;
                    max = 2000;
                }
                // get exercises within a specific lexile measure diffilcuty
                _exercises = new Queue<Exercise>(await _exerciseService.GetShuffledWithinDifficulty(min, max));
            }

            string text; // text of the new exercise
            if (_exercises.Count > 0)
            {
                Exercise exercise = _exercises.Dequeue(); // new exercise
                text = exercise.Text;
                _viewModel.ExerciseId = exercise.Id;
            }
            else
            {
                text = "This is an example text which is shown when the database contains no data.";
                _viewModel.ExerciseId = 1;
            }
            _viewModel.ExerciseText = text;
        }
    }
}
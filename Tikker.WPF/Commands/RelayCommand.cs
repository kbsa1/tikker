﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Tikker.WPF.Commands
{
    /// <summary>
    /// A relay command is used to create commands without creating a new class.
    /// Source: https://www.technical-recipes.com/2016/using-relaycommand-icommand-to-handle-events-in-wpf-and-mvvm/
    /// </summary>
    /// <example>
    /// private RelayCommand _loadCategoryCommand = new RelayCommand(ExecuteLoadCategoryCommand);
    /// private void ExecuteLoadCategoryCommand(object sender)
    /// {
    ///     // do something
    /// }
    /// </example>
    /// <example>
    /// private RelayCommand _loadCommand = new RelayCommand(ExecuteLoadCommand, CanExecuteLoadCommand);
    /// private void ExecuteLoadCommand(object sender)
    /// {
    ///     // do something
    /// }
    /// private bool CanExecuteLoadCommand(object sender)
    /// {
    ///     return true;
    /// }
    /// </example>
    public class RelayCommand : ICommand
    {
        private readonly Action<object> _execute;
        private readonly Func<object, bool> _canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}

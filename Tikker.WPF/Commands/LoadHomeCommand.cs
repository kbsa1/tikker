﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Tikker.Domain.Models;
using Tikker.Domain.Services;
using Tikker.EntityFramework;
using Tikker.EntityFramework.Services;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Commands
{
    /// <summary>
    /// command ran when the homepage is loaded.
    /// Used to run database connections async.
    /// </summary>
    public class LoadHomeCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// homeviewmodel. used to alter values based on things gotten from the database.
        /// </summary>
        private HomeViewModel _homeViewmodel;

        /// <summary>
        /// statistics service.
        /// </summary>
        private IDataService<Statistic> _statisticsService = new GenericDataService<Statistic>(new TikkerDbContextFactory());

        public LoadHomeCommand(HomeViewModel homeViewModel)
        {
            _homeViewmodel = homeViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            // get all statistics and use it to calculate average WPM
            List<Statistic> statistics = (List<Statistic>)await _statisticsService.GetAll();
            if(statistics.Count != 0)
            {
                // calculate average WPM
                int sumWPM = 0;
                foreach (Statistic statistic in statistics)
                {
                    sumWPM += statistic.WPM;
                }
                int averageWPM = sumWPM / statistics.Count;

                // update viewmodel
                _homeViewmodel.AvgWPM = averageWPM.ToString() + " WPM";
                _homeViewmodel.StatisticsButtonIsEnabled = true; // enable statistics button
            }
            else
            {
                _homeViewmodel.StatisticsButtonIsEnabled = false; // disable statistics button
            }
        }
    }
}

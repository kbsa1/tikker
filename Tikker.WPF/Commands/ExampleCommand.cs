﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Tikker.Domain.Models;
using Tikker.Domain.Services;
using Tikker.EntityFramework;
using Tikker.EntityFramework.Services;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Commands
{
    /// <summary>
    /// command with some example code.
    /// Once shown to the devs internally during a showcase.
    /// We kept the file for reference.
    /// </summary>
    public class ExampleCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private ExampleViewModel _viewModel;

        public ExampleCommand(ExampleViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            // creating a service requires a ContextFactory instance to be given.
            // the GenericDataService is a generic class that takes a model as type.
            IDataService<User> userService = new GenericDataService<User>(new TikkerDbContextFactory());
            IDataService<Exercise> exerciseService = new GenericDataService<Exercise>(new TikkerDbContextFactory());
            IDataService<Statistic> statisticService = new GenericDataService<Statistic>(new TikkerDbContextFactory());


            // create statistics
            //User user = await userService.Get(1);
            //Exercise exercise = await exerciseService.Get(1);
            //Statistic statistic1 = new Statistic()
            //{
            //    Accuracy = 98,
            //    WPM = 60,
            //    Score = 80,
            //    Time = TimeSpan.FromSeconds(50),
            //    TakenOn = DateTime.Now,
            //    User = user,
            //    Exercise = exercise,
            //};
            //Statistic statistic2 = new Statistic()
            //{
            //    Accuracy = 70,
            //    WPM = 90,
            //    Score = 70,
            //    Time = TimeSpan.FromSeconds(30),
            //    TakenOn = DateTime.Now,
            //    User = user,
            //    Exercise = exercise,
            //};
            //await statisticService.Create(statistic1);
            //await statisticService.Create(statistic2);

            // IDataService<Exercise> exerciseService = new GenericDataService<Exercise>(new TikkerDbContextFactory());

            //await exerciseService.Delete(7);
            //_viewModel.Result = _viewModel.UserInput.ToUpper();
            Exercise exercise = await exerciseService.Get(7);
            //exercise.Text = "7. Oefentekst om te typen!";
            //await exerciseService.Update(7, exercise);
            //_viewModel.Result = exercise.Text;

            //Exercise exerciseToCreate = new Exercise
            //{
            //    Text = "5. Oefentekst om te typen!",
            //    Difficulty = 3
            //};

            //Exercise createdExercise = await exerciseService.Create(exerciseToCreate);
            //_viewModel.Result = createdExercise.Id.ToString();
        }
    }
}

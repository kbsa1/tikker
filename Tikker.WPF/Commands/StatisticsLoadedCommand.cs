﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Tikker.Domain.Models;
using Tikker.Domain.Services;
using Tikker.EntityFramework;
using Tikker.EntityFramework.Services;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Commands
{
    /// <summary>
    /// command ran when the statistics is loaded and to prepare all data async.
    /// </summary>
    class StatisticsLoadedCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// statistics service to get performance data from
        /// </summary>
        public IStatisticsService StatisticsService { get; set; }
        /// <summary>
        /// viewmodel to change the data of
        /// </summary>
        private StatisticsViewModel _viewModel;
        
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="statisticsViewModel"></param>
        public StatisticsLoadedCommand(StatisticsViewModel statisticsViewModel)
        {
            _viewModel = statisticsViewModel;
            StatisticsService = new StatisticsService(new TikkerDbContextFactory());
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            _viewModel.Statistics = (List<Statistic>)await StatisticsService.GetAll();
            if (_viewModel.Statistics.Count != 0)
            {
                // get difficulty category
                DifficultyCategory difficultyCategory = await StatisticsService.GetDifficultyCategory();

                // calculate average WPM
                int sumWPM = 0;
                foreach (Statistic statistic in _viewModel.Statistics)
                {
                    sumWPM += statistic.WPM;
                }
                int averageWPM = sumWPM / _viewModel.Statistics.Count;

                // calculate average accuracy
                int sumAccuracy = 0;
                foreach (Statistic statistic in _viewModel.Statistics)
                {
                    sumAccuracy += statistic.Accuracy;
                }
                int averageAccuracy = sumAccuracy / _viewModel.Statistics.Count;

                // calculate average score
                int sumScore = 0;
                foreach (Statistic statistic in _viewModel.Statistics)
                {
                    sumScore += statistic.Score;
                }
                int averageScore = sumScore / _viewModel.Statistics.Count;

                // update properties
                _viewModel.DifficultyCategory = "Text difficulty based on performance: " + difficultyCategory.ToString();
                _viewModel.AvgWPM = "Average typing speed: " + averageWPM.ToString() + " WPM";
                _viewModel.AvgAccuracy = "Average accuracy: " + averageAccuracy.ToString() + "%";
                _viewModel.AvgScore = "Average score: " + averageScore.ToString();
            }

            // set the viewmodel as loaded
            _viewModel.Loaded = true;
        }


    }
}


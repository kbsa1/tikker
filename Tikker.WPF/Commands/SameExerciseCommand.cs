﻿using System;
using System.Windows.Input;
using Tikker.EntityFramework;
using Tikker.EntityFramework.Services;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Commands
{
    /// <summary>
    /// command used to repeat the current exercise again async.
    /// </summary>
    public class SameExerciseCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private ExerciseViewModel _viewModel;

        public SameExerciseCommand(ExerciseViewModel exerciseViewModel)
        {
            _viewModel = exerciseViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            _viewModel.ExerciseText = _viewModel.ExerciseText;
        }
    }
}
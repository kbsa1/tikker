using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace Tikker.WPF.Views
{
    /// <summary>
    /// Handles the creation and removal of popups
    /// </summary>
    public class PopupManager
    {
        public ObservableCollection<Popup> Popups { get; }
        
        private const int PopupHeight = 50;
        private const int PopupWidth = 500;
        private const int PopupMargin = 10;
        
        public PopupManager()
        {
            Popups = new ObservableCollection<Popup>();
        }

        /// <summary>
        /// Show a popup for a given amount of time
        /// </summary>
        /// <param name="title">First line of text</param>
        /// <param name="text">Second line of text</param>
        /// <param name="duration">The amount of time for which the popup should be visible</param>
        /// <param name="iconPath">The path to the icon that should be displayed in the popup</param>
        public void ShowPopup(string title, string text, TimeSpan duration, string iconPath)
        {
            int startOffset = Popups.Count * (PopupHeight + PopupMargin);
            Popups.Add(new Popup(title, text, startOffset, PopupWidth, PopupHeight, duration, iconPath));
        }

        /// <summary>
        /// Remove popups after duration time has passed
        /// </summary>
        public void RemovePopups()
        {
            for (int i = Popups.Count - 1; i >= 0; i--)
            {
                
                if (Popups[i].ShouldRemove())
                {
                    int i1 = i;
                    Application.Current.Dispatcher.Invoke( () =>
                    {
                        Popups[i1].IsOpen = false;
                        Popups.RemoveAt(i1);
                    });
                }
            }
        }
    }
}
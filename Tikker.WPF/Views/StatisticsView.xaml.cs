﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Views
{
    /// <summary>
    /// Interaction logic for StatisticsView.xaml
    /// </summary>
    public partial class StatisticsView : UserControl
    { 
        private StatisticsViewModel _viewModel;
        public StatisticsView()
        {
            InitializeComponent();
            DataContextChanged += delegate
            {
                _viewModel = (StatisticsViewModel)(this.DataContext);
                if (_viewModel != null)
                {
                    _viewModel.PropertyChanged += delegate(object sender, PropertyChangedEventArgs args)
                    {
                        if (args.PropertyName == nameof(_viewModel.Statistics))
                        {
                            LoadDataGrid();
                        }
                    };
                }
            };
            
        }

        private void LoadDataGrid()
        {
            dg.ItemsSource = _viewModel.Statistics;              
        }

        
    }
}

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace Tikker.WPF.Views
{
    /// <summary>
    /// Represents a popup that is displayed in the bottom right corner of the screen
    /// </summary>
    public class Popup : INotifyPropertyChanged
    {
        public int Width { get; }
        public int Height { get; }
        
        public event PropertyChangedEventHandler PropertyChanged;
        
        private string _text;
        public string Text
        {
            get => _text;
            set
            {
                _text = value;
                OnPropertyChanged(nameof(Text));
            }
        }
        
        private string _title;
        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged(nameof(Title));
            }
        }
        
        private double _offsetHorizontal;
        public double OffsetHorizontal
        {
            get => _offsetHorizontal;
            set { 
                _offsetHorizontal = value;
                OnPropertyChanged(nameof(OffsetHorizontal));
            }
        }

        private double _offsetVertical;
        public double OffsetVertical
        {
            get => _offsetVertical;
            set { 
                _offsetVertical = value;
                OnPropertyChanged(nameof(OffsetVertical));
            }
        }
        
        private bool _isOpen;
        public bool IsOpen
        {
            get => _isOpen;
            set
            {
                _isOpen = value;
                OnPropertyChanged(nameof(IsOpen));
                OnPropertyChanged(nameof(Animation));
            }
        }

        private string _iconPath;
        public string IconPath
        {
            get => _iconPath;
            set
            {
                _iconPath = value;
                OnPropertyChanged(nameof(IconPath));
            }
        }

        public PopupAnimation Animation => IsOpen ? PopupAnimation.Fade : PopupAnimation.Slide;

        private readonly int _startOffset;
        private DateTime _startTime;
        private TimeSpan _duration;

        /// <summary>
        /// Initializes a new popup to be displayed immediately
        /// </summary>
        /// <param name="title">The first line of text</param>
        /// <param name="text">The second line of text</param>
        /// <param name="startOffset">The vertical offset from the previous popup</param>
        /// <param name="width">Width of the popup</param>
        /// <param name="height">Height of the popup</param>
        /// <param name="duration">How long the popup should be visible</param>
        /// <param name="iconPath">The path to the icon that should be displayed</param>
        public Popup(string title, string text, int startOffset, int width, int height, TimeSpan duration, string iconPath)
        {
            Title = title;
            Text = text;
            IsOpen = true;
            Width = width;
            Height = height;
            IconPath = iconPath;
            _startOffset = startOffset;
            _duration = duration;
            _startTime = DateTime.Now;
            
            if (Application.Current.MainWindow != null)
            {
                Application.Current.MainWindow.LocationChanged += OnWindowChanged;
                Application.Current.MainWindow.SizeChanged += OnWindowChanged;
                OnWindowChanged(Application.Current.MainWindow, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Returns true when popup is not open and duration time has passed.
        /// </summary>
        /// <returns></returns>
        public bool ShouldRemove()
        {
            return IsOpen && DateTime.Now >= _startTime + _duration;
        }
        
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        
        /// <summary>
        /// Handles the Window LocationChanged and SizeChanged events.
        /// Repositions the popup.
        /// </summary>
        /// <param name="sender">Should be a MainWindow</param>
        /// <param name="e">Not used</param>
        private void OnWindowChanged(object sender, EventArgs e)
        {
            if (sender is MainWindow window)
            {
                // "bump" the offset to cause the popup to reposition itself
                //  on its own
                OffsetHorizontal = window.Width - 16;
                OffsetHorizontal = window.Width - 17;
                OffsetVertical = window.Height - 100 - _startOffset;
            }
        }
    }
}
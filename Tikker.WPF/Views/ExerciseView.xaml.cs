using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using SpeechLib;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Views
{
    /// <summary>
    /// Interaction logic for ExerciseView.xaml
    /// </summary>
    public partial class ExerciseView : UserControl
    {
        private ExerciseViewModel _exerciseViewModel;
        private SolidColorBrush _backgroundDarkBrush;
        private SolidColorBrush _primaryBrush;

        /// <summary>
        /// A dictionary where the key is a keycode string
        /// and the value is the Rectangle belonging to it.
        /// Used to know which key must be pressed to highlight the corresponding Rectangle.
        /// </summary>
        public Dictionary<string, Rectangle> KeyboardDict;

        /// <summary>
        /// When a new key is being highlighted, the previous key must be un-highlighted.
        /// That's why it's stored here.
        /// </summary>
        private string _previousKey { get; set; }

        public ExerciseView()
        {
            InitializeComponent();
            InitialiseKeyboard();
        }
        /// <summary>
        /// Initialises the keyboard
        /// </summary>
        private void InitialiseKeyboard()
        {
            KeyboardDict = GetKeyboardDictionary();
            _backgroundDarkBrush = (SolidColorBrush)this.TryFindResource("BackgroundDarkBrush");
            _primaryBrush = (SolidColorBrush)this.TryFindResource("PrimaryBrush");

            // viewmodel is stored once it's set
            DataContextChanged += delegate
            {
                // only bind events when dataContext is set, not unset
                if(this.DataContext == null)
                {
                    return;
                }
                _exerciseViewModel = (ExerciseViewModel)(this.DataContext);
                
                // events on viewmodel
                _exerciseViewModel.PropertyChanged += delegate (object sender, PropertyChangedEventArgs args)
                {
                    // When exercise text is changed, show it here
                    if (args.PropertyName == nameof(_exerciseViewModel.ExerciseText)
                    || args.PropertyName == nameof(_exerciseViewModel.CurrentWord)
                    || args.PropertyName == nameof(_exerciseViewModel.ExerciseInput))
                    {
                        RenderExerciseText();
                    }
                    // when key to type is changed, change the highlighting of the keyboard
                    else if (!_exerciseViewModel.KeyboardIsCollapsed & args.PropertyName == nameof(_exerciseViewModel.KeyToType))
                    {
                        HighlightKey(_exerciseViewModel.KeyToType);
                    }
                };
            };
        }

        /// <summary>
        /// Show the exercisetext with the appropriate colours
        /// </summary>
        private void RenderExerciseText()
        {
            ExerciseTextBlock.Inlines.Clear();
            // if its empty nothing has to be written
            if (_exerciseViewModel.Words == null || _exerciseViewModel.Words.Count < 1)
            {
                return;
            }

            // render text
            LinkedListNode<string> current = _exerciseViewModel.Words.First;
            
            bool reachedCurrentWord = false;
            // loop through each word
            do
            {
                // register when the current word has been reached
                // because every word before it is 100% correctly typed
                if(current == _exerciseViewModel.CurrentWord)
                {
                    reachedCurrentWord = true;
                }

                // for current words we have to check each individual letter
                // if it's correct or not
                if (current == _exerciseViewModel.CurrentWord)
                {
                    // for each letter
                    for (int i = 0; i < current.Value.Length; ++i)
                    {
                        Brush foreground = ExerciseTextBlock.Foreground;
                        Brush background = null;
                        // we do an extra if check to ensure input isnt bigger than the word,
                        // which would cause an index out of bounds error.
                        if (_exerciseViewModel.ExerciseInput.Length-1 >= i)
                        {
                            // if letter is correct, colour green. else colour it red.
                            if (_exerciseViewModel.ExerciseInput[i] == current.Value[i])
                            {
                                int a = i + 1;
                                if (a < current.Value.Length && _exerciseViewModel.IncorrectCharactersInWord == 0)
                                {
                                    string keyToType = _exerciseViewModel.Key(current.Value[a].ToString());
                                    _exerciseViewModel.CaseSensitiveKeyToType = current.Value[a].ToString();
                                    _exerciseViewModel.KeyToType = keyToType;
                                }
                                foreground = Brushes.Green;


                            } else
                            {
                                background = Brushes.Red;
                            }
                        }
                        ExerciseTextBlock.Inlines.Add(new Run(current.Value[i].ToString())
                        {
                            Foreground = foreground,
                            Background = background,
                            // underline current word, but dont underline the last space
                            TextDecorations = (i >= current.Value.Length - 1) ? null : TextDecorations.Underline,
                        });
                    }
                }
                // for words that aren't the current word we can easily apply the logic:
                // any word before the current word is correctly typed,
                // any word after the current word still needs to be typed
                else
                {
                    Brush foreground = ExerciseTextBlock.Foreground;
                    // green for correctly typed words
                    if (!reachedCurrentWord)
                    {
                        foreground = Brushes.Green;
                    }
                    ExerciseTextBlock.Inlines.Add(new Run(current.Value)
                    {
                        Foreground = foreground
                    });
                    
                }
                

                current = current.Next;
            } while (current != null);

            //string correct = "";
            //string incorrect = "";
            //string unwritten = "";
            //ExerciseTextBlock.Inlines.Add(new Run(correct) { Foreground = Brushes.Green });
            //ExerciseTextBlock.Inlines.Add(new Run(incorrect) { Background = Brushes.Red });
            //ExerciseTextBlock.Inlines.Add(new Run(unwritten));
            if (_exerciseViewModel.IncorrectCharactersInWord > 0)
            {
                if (!_exerciseViewModel.IncorrectWordsList.Contains(_exerciseViewModel.CurrentWord))
                {
                    
                    _exerciseViewModel.IncorrectWordsList.Add(_exerciseViewModel.CurrentWord);
                    _exerciseViewModel.IncorrectWordsListWithOutSpecialCharacters.Add(RemoveSpecialCharacters(_exerciseViewModel.CurrentWord));
                }

                _exerciseViewModel.SumWrongLetters += 1;
                
                
                _exerciseViewModel.KeyToType = "Key_Back";
            }
            if (_exerciseViewModel.ExerciseInput.Length == 0)
            {
                string keyToType = _exerciseViewModel.Key(_exerciseViewModel.CurrentWord.Value[0].ToString());
                _exerciseViewModel.CaseSensitiveKeyToType = _exerciseViewModel.CurrentWord.Value[0].ToString();
                _exerciseViewModel.KeyToType = keyToType;
            }
        }

        /// <summary>
        /// Highlights the key that has to be typed on the visual keyboard.
        /// </summary>
        /// <param name="key"></param>
        private void HighlightKey(string key)
        {
            // key highlights
            if (_previousKey != null) // if there is a previous key
            {
                bool isInDictionary = KeyboardDict.TryGetValue(_previousKey, out Rectangle rec1); // get previous key from dictionary
                if (isInDictionary) // if the previous key is in the dictionary
                {
                    rec1.Fill = _backgroundDarkBrush; // set previous key back to default key color
                }
            }

            bool isInDictionary2 = KeyboardDict.TryGetValue(key, out Rectangle rec2); // get key to type from dictionary
            if (isInDictionary2) // if the key to type is in the dictionary
            {
                rec2.Fill = _primaryBrush; // highlight the key

                bool shiftIsInDictionary = KeyboardDict.TryGetValue("Key_LeftShift", out Rectangle recShift); // get shift key from dictionary

                if (CheckIfKeyIsCapitalLetter(_exerciseViewModel.CaseSensitiveKeyToType)
                ) // if the key to type is a capital letter
                {
                    if (shiftIsInDictionary) // if the shift key is in the dictionary
                    {
                        recShift.Fill = _primaryBrush; // highlight the shift key
                    }
                }
                else
                {
                    if (shiftIsInDictionary)
                    {
                        recShift.Fill = _backgroundDarkBrush;
                    }
                }
            }

            _previousKey = key;
        }

        /// <summary>
        /// Returns true if the last character of a key is a capital letter
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private bool CheckIfKeyIsCapitalLetter(string key)
        {
            char lastCharacter = key[^1]; // get last character
            if (Char.IsUpper(lastCharacter))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// When the keyboard collapse button is pressed,
        /// toggle the visibility of the keyboard.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonKbCollapse_Click(object sender, RoutedEventArgs e)
        {
            _exerciseViewModel.KeyboardIsCollapsed = !_exerciseViewModel.KeyboardIsCollapsed; // if boolean was true, make it false and vice versa
        }

        /// <summary>
        /// Gets a dictionary where the key is a keycode string
        /// and the value is the Rectangle belonging to it
        /// </summary>
        /// <returns>dictionary with keycodes and rectangles</returns>
        private Dictionary<string, Rectangle> GetKeyboardDictionary()
        {
            Dictionary<string, Rectangle> keyboardDict = new Dictionary<string, Rectangle>();
            keyboardDict.Add("Key_DeadCharProcessed", Key_Oem3);
            // first row
            keyboardDict.Add("Key_Oem3", Key_Oem3);
            keyboardDict.Add("Key_D1", Key_D1);
            keyboardDict.Add("Key_D2", Key_D2);
            keyboardDict.Add("Key_D3", Key_D3);
            keyboardDict.Add("Key_D4", Key_D4);
            keyboardDict.Add("Key_D5", Key_D5);
            keyboardDict.Add("Key_D6", Key_D6);
            keyboardDict.Add("Key_D7", Key_D7);
            keyboardDict.Add("Key_D8", Key_D8);
            keyboardDict.Add("Key_D9", Key_D9);
            keyboardDict.Add("Key_D0", Key_D0);
            keyboardDict.Add("Key_OemMinus", Key_OemMinus);
            keyboardDict.Add("Key_OemPlus", Key_OemPlus);
            keyboardDict.Add("Key_Back", Key_Back);

            // second row
            keyboardDict.Add("Key_Tab", Key_Tab);
            keyboardDict.Add("Key_Q", Key_Q);
            keyboardDict.Add("Key_W", Key_W);
            keyboardDict.Add("Key_E", Key_E);
            keyboardDict.Add("Key_R", Key_R);
            keyboardDict.Add("Key_T", Key_T);
            keyboardDict.Add("Key_Y", Key_Y);
            keyboardDict.Add("Key_U", Key_U);
            keyboardDict.Add("Key_I", Key_I);
            keyboardDict.Add("Key_O", Key_O);
            keyboardDict.Add("Key_P", Key_P);
            keyboardDict.Add("Key_OemOpenBrackets", Key_OemOpenBrackets);
            keyboardDict.Add("Key_Oem6", Key_Oem6);
            keyboardDict.Add("Key_Oem5", Key_Oem5);

            // third row
            keyboardDict.Add("Key_Capital", Key_Capital);
            keyboardDict.Add("Key_A", Key_A);
            keyboardDict.Add("Key_S", Key_S);
            keyboardDict.Add("Key_D", Key_D);
            keyboardDict.Add("Key_F", Key_F);
            keyboardDict.Add("Key_G", Key_G);
            keyboardDict.Add("Key_H", Key_H);
            keyboardDict.Add("Key_J", Key_J);
            keyboardDict.Add("Key_K", Key_K);
            keyboardDict.Add("Key_L", Key_L);
            keyboardDict.Add("Key_Oem1", Key_Oem1);
            keyboardDict.Add("Key_OemQuotes", Key_OemQuotes);
            keyboardDict.Add("Key_Return", Key_Return);

            // fourth row
            keyboardDict.Add("Key_LeftShift", Key_LeftShift);
            keyboardDict.Add("Key_Z", Key_Z);
            keyboardDict.Add("Key_X", Key_X);
            keyboardDict.Add("Key_C", Key_C);
            keyboardDict.Add("Key_V", Key_V);
            keyboardDict.Add("Key_B", Key_B);
            keyboardDict.Add("Key_N", Key_N);
            keyboardDict.Add("Key_M", Key_M);
            keyboardDict.Add("Key_OemComma", Key_OemComma);
            keyboardDict.Add("Key_OemPeriod", Key_OemPeriod);
            keyboardDict.Add("Key_OemQuestion", Key_OemQuestion);
            keyboardDict.Add("Key_RightShift", Key_RightShift);

            // fifth row
            keyboardDict.Add("Key_Space", Key_Space);

            return keyboardDict;
        }

        /// <summary>
        /// Tells the ExerciseViewModel class to enable TTS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonTTS_Click(object sender, RoutedEventArgs e)
        {
            _exerciseViewModel.TTSEnabled = !_exerciseViewModel.TTSEnabled;
        }

        /// <summary>
        /// Removes all special characters from a LinkedListNode string.
        /// Used for the incorrect words list
        /// </summary>
        /// <param name="nodeStr"></param>
        /// <returns></returns>
        public static LinkedListNode<string> RemoveSpecialCharacters(LinkedListNode<string> nodeStr)
        {
            StringBuilder sb = new StringBuilder();
            string str = nodeStr.Value;
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] >= '0' && str[i] <= '9')
                    || (str[i] >= 'A' && str[i] <= 'z'))
                {
                    sb.Append(str[i]);
                }
            }

            return new LinkedListNode<string>(sb.ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tikker.Domain.Models;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Views
{
    /// <summary>
    /// Interaction logic for GraphView.xaml
    /// </summary>
    public partial class GraphView : UserControl
    {
        /// <summary>
        /// amount of space used for labels on the left
        /// </summary>
        private const double _labelOffsetX = 25d;
        /// <summary>
        /// amount of space used for labels on the bottom
        /// </summary>
        private const double _labelOffsetY = 25d;
        /// <summary>
        /// Font size of the value labels
        /// </summary>
        private const int _labelFontSize = 12;
        /// <summary>
        /// the size of the circle of each point on the graph
        /// </summary>
        private const int _pointSize = 15;
        /// <summary>
        /// how thick a line is
        /// </summary>
        private const int _lineThicccness = 2;

        /// <summary>
        /// background colour of canvas
        /// </summary>
        private readonly SolidColorBrush _backgroundColour;
        /// <summary>
        /// colour of lines in background
        /// </summary>
        private readonly SolidColorBrush _gridColour;
        /// <summary>
        /// colour of the labels
        /// </summary>
        private readonly SolidColorBrush _labelColour;
        /// <summary>
        /// colour of the outline of each point of the graph
        /// </summary>
        private readonly SolidColorBrush _lineColour;
        /// <summary>
        /// colour of the inside of each point on the graph
        /// </summary>
        private readonly SolidColorBrush _pointInnerColour;

        /// <summary>
        /// viewmodel containing data for this view
        /// </summary>
        private StatisticsViewModel _viewModel;

        public GraphView()
        {
            // init colours
            _backgroundColour = new SolidColorBrush(Colors.White);
            _gridColour = new SolidColorBrush(Colors.Gray);
            _labelColour = new SolidColorBrush(Colors.White);
            _lineColour = (SolidColorBrush)Application.Current.Resources["PrimaryBrush"];
            _pointInnerColour = new SolidColorBrush(Colors.White);
            InitializeComponent();

            // on viewmodel set
            DataContextChanged += delegate
            {
                // ensure is set and not unset
                if(this.DataContext == null)
                {
                    return;
                }
                _viewModel = (StatisticsViewModel)(this.DataContext);
                _viewModel.PropertyChanged += delegate (object sender, PropertyChangedEventArgs args)
                {
                    // on viewmodel loaded
                    if (args.PropertyName == nameof(_viewModel.Loaded) && _viewModel.Loaded == true)
                    {
                        InitGraph();
                    }
                };
            };
        }

        /// <summary>
        /// initialise the canvas by getting data and drawing graph with it
        /// </summary>
        public void InitGraph()
        {
            // no need to draw a graph when there's barely any data
            if(_viewModel.Statistics.Count < 20)
            {
                return;
            }
            int xAmount = 10; // how many points are on the x axis
            int yAmount = 9; // how many points are on the y axis

            // because of the rounding not all statistics can be shown
            // offset is added to to not-show earlier performed statistics instead of new ones
            int offset = (_viewModel.Statistics.Count % xAmount);
            // amount of statistics to actually use
            // is rounded to xAmount, which sadly might leave out recent statistics from the user
            // but if this is not done the points won't line up with the grid or match the labels.
            int statisticsCount = _viewModel.Statistics.Count - offset;
            int xStep = statisticsCount / xAmount;

            // create points based on statistics data
            ICollection<Vector2> points = new List<Vector2>();
            for(int i=1; i<=xAmount; ++i)
            {
                int index = i * xStep + offset;
                // -1 because xStep is based on count but index starts at 0
                Statistic statistic = _viewModel.Statistics[index-1];
                points.Add(new Vector2(index, statistic.Score));
            }

            DrawGraph(points, xAmount, yAmount);
            //DrawGrid(12, 9);
            //DrawLabels(0, 0, 198, 160, 12, 9);
            //DrawPointsAndLines(new List<Vector2>()
            //{
            //    new Vector2(0, 30),
            //    new Vector2(18, 80),
            //    new Vector2(36, 75),
            //    new Vector2(54, 30),
            //    new Vector2(72, 70),
            //    new Vector2(90, 115),
            //    new Vector2(108, 60),
            //    new Vector2(126, 95),
            //    new Vector2(144, 130),
            //    new Vector2(162, 115),
            //    new Vector2(180, 90),
            //    new Vector2(198, 150),
            //},
            //0, 0, 198, 160);
        }

        /// <summary>
        /// draw the given points spread over x and y amount of points
        /// </summary>
        /// <param name="points"></param>
        /// <param name="xAmount"></param>
        /// <param name="yAmount"></param>
        public void DrawGraph(ICollection<Vector2> points, int xAmount, int yAmount)
        {
            DrawGrid(xAmount, yAmount);
            double maxX = GetVector2WithHighestX(points).X;
            double maxY = GetVector2WithHighestY(points).Y;
            double minX = GetVector2WithLowestX(points).X;
            double minY = GetVector2WithLowestY(points).Y;
            DrawLabels(minX, minY, maxX, maxY, xAmount, yAmount);
            DrawPointsAndLines(points, minX, minY, maxX, maxY);
        }

        /// <summary>
        /// returns the vector2 with the highest X property in a collection of Vector2s
        /// </summary>
        /// <param name="points">vector2s to compare</param>
        /// <returns></returns>
        private Vector2 GetVector2WithHighestX(ICollection<Vector2> points)
        {
            if(points.Count < 1)
            {
                return null;
            }
            Vector2 highest = Vector2.Zero;
            foreach(Vector2 point in points)
            {
                if(point.X > highest.X)
                {
                    highest = point;
                }
            }
            return highest;
        }

        /// <summary>
        /// returns the vector2 with the highest Y property in a collection of Vector2s
        /// </summary>
        /// <param name="points">vector2s to compare</param>
        /// <returns></returns>
        private Vector2 GetVector2WithHighestY(ICollection<Vector2> points)
        {
            if (points.Count < 1)
            {
                return null;
            }
            Vector2 highest = Vector2.Zero;
            foreach (Vector2 point in points)
            {
                if (point.Y > highest.Y)
                {
                    highest = point;
                }
            }
            return highest;
        }

        /// <summary>
        /// returns the vector2 with the lowest X property in a collection of Vector2s
        /// </summary>
        /// <param name="points">vector2s to compare</param>
        /// <returns></returns>
        private Vector2 GetVector2WithLowestX(ICollection<Vector2> points)
        {
            if (points.Count < 1)
            {
                return null;
            }
            Vector2 lowest = Vector2.Max;
            foreach (Vector2 point in points)
            {
                if (point.X < lowest.X)
                {
                    lowest = point;
                }
            }
            return lowest;
        }

        /// <summary>
        /// returns the vector2 with the lowest Y property in a collection of Vector2s
        /// </summary>
        /// <param name="points">vector2s to compare</param>
        /// <returns></returns>
        private Vector2 GetVector2WithLowestY(ICollection<Vector2> points)
        {
            if (points.Count < 1)
            {
                return null;
            }
            Vector2 lowest = Vector2.Max;
            foreach (Vector2 point in points)
            {
                if (point.Y < lowest.Y)
                {
                    lowest = point;
                }
            }
            return lowest;
        }

        /// <summary>
        /// draw points and lines
        /// </summary>
        /// <param name="points">points to draw</param>
        /// <param name="xMin">the lowest value of the x axis</param>
        /// <param name="yMin">the lowest value of the y axis</param>
        /// <param name="xMax">the highest value of the x axis</param>
        /// <param name="yMax">the highest value of the y axis</param>
        private void DrawPointsAndLines(ICollection<Vector2> points, double xMin, double yMin, double xMax, double yMax)
        {
            IList<Vector2> pointPositions = new List<Vector2>();

            // calculate positions on graph
            foreach (Vector2 point in points)
            {
                double xValuePercentage = 100 / (xMax - xMin) * (point.X - xMin);
                double xPositionOnGraph = (GraphCanvas.ActualWidth - _labelOffsetX) / 100 * xValuePercentage;
                double xPosition = xPositionOnGraph + _labelOffsetX;

                double yValuePercentage = 100 / (yMax - yMin) * (point.Y - yMin);
                double yPositionOnGraph = (GraphCanvas.ActualHeight - _labelOffsetY) / 100 * yValuePercentage;
                double yPosition = GraphCanvas.ActualHeight - _labelOffsetY - yPositionOnGraph;

                // add position to list
                pointPositions.Add(new Vector2(xPosition, yPosition));
            }

            // sort by x position
            pointPositions = pointPositions.OrderBy(p => p.X).ToList();

            //draw lines
            for(int i=1; i<pointPositions.Count; ++i)
            {
                Vector2 from = pointPositions[i - 1];
                Vector2 to = pointPositions[i];
                Line line = new Line();
                line.Stroke = _lineColour;
                line.StrokeThickness = _lineThicccness;
                line.X1 = from.X;
                line.Y1 = from.Y;
                line.X2 = to.X;
                line.Y2 = to.Y;
                GraphCanvas.Children.Add(line);
            }

            // draw circles
            foreach (Vector2 point in pointPositions)
            {
                Ellipse circle = new Ellipse();
                circle.Width = _pointSize;
                circle.Height = _pointSize;
                circle.Fill = _pointInnerColour;
                circle.Stroke = _lineColour;
                circle.StrokeThickness = _lineThicccness;
                Canvas.SetLeft(circle, point.X - (_pointSize / 2));
                Canvas.SetTop(circle, point.Y - (_pointSize / 2));
                GraphCanvas.Children.Add(circle);
            }
        }

        /// <summary>
        /// Draw the labels on the left and bottom of the graph
        /// </summary>
        /// <param name="xMin">the lowest value of the x axis</param>
        /// <param name="yMin">the lowest value of the y axis</param>
        /// <param name="xMax">the highest value of the x axis</param>
        /// <param name="yMax">the highest value of the y axis</param>
        /// <param name="xAmount">amount of steps to divide the x axis in</param>
        /// <param name="yAmount">amount of steps to divide the y axis in</param>
        private void DrawLabels(double xMin, double yMin, double xMax, double yMax, int xAmount, int yAmount)
        {
            // labels on bottom
            double xValueStep = (xMax - xMin) / (xAmount-1);
            Double xValue = xMin;
            double xPositionStep = (GraphCanvas.ActualWidth - _labelOffsetX) / (xAmount-1);
            double xPos = _labelOffsetX - (_labelFontSize/2);
            double yPos = GraphCanvas.ActualHeight - _labelOffsetY;
            for (int x = 0; x < xAmount; ++x)
            {
                TextBlock textBlock = GetText(xPos, yPos, xValue.ToString("N0"), _labelColour);
                textBlock.FontSize = _labelFontSize;
                textBlock.TextAlignment = TextAlignment.Center;
                GraphCanvas.Children.Add(textBlock);
                xPos += xPositionStep;
                xValue += xValueStep;
            }

            // labels on left
            double yValueStep = (yMax - yMin) / (yAmount - 1);
            Double yValue = yMax;
            double yPositionStep = (GraphCanvas.ActualHeight - _labelOffsetY) / (yAmount - 1);
            yPos = 0;
            for (int y = 0; y < yAmount; ++y)
            {
                TextBlock textBlock = GetText(0, yPos, yValue.ToString("N0"), _labelColour);
                textBlock.FontSize = _labelFontSize;
                textBlock.TextAlignment = TextAlignment.Right;
                GraphCanvas.Children.Add(textBlock);
                yPos += yPositionStep;
                yValue -= yValueStep;
            }
        }

        /// <summary>
        /// draw background grid
        /// </summary>
        /// <param name="xAmount"></param>
        /// <param name="yAmount"></param>
        private void DrawGrid(int xAmount, int yAmount)
        {
            // background
            Rectangle background = new Rectangle();
            background.Fill = _backgroundColour;
            background.Width = GraphCanvas.ActualWidth - _labelOffsetX;
            background.Height = GraphCanvas.ActualHeight - _labelOffsetY;
            Canvas.SetLeft(background, _labelOffsetX);
            Canvas.SetTop(background, 0);
            GraphCanvas.Children.Add(background);

            // horizontal lines
            double yStep = (GraphCanvas.ActualHeight - _labelOffsetY) / (yAmount-1);
            double yPos = 0;
            for (int y = 0; y < yAmount; ++y)
            {
                yPos += yStep;
                Line line = new Line();
                line.Stroke = _gridColour;
                line.StrokeThickness = 1;
                line.X1 = _labelOffsetX;
                line.Y1 = yPos;
                line.X2 = GraphCanvas.ActualWidth;
                line.Y2 = yPos;
                GraphCanvas.Children.Add(line);
            }

            // vertical lines
            double xStep = (GraphCanvas.ActualWidth - _labelOffsetX) / (xAmount-1);
            double xPos = _labelOffsetX;
            for (int x = 0; x < xAmount; ++x)
            {
                Line line = new Line();
                line.Stroke = _gridColour;
                line.StrokeThickness = 1;
                line.X1 = xPos;
                line.Y1 = 0;
                line.X2 = xPos;
                line.Y2 = GraphCanvas.ActualHeight - _labelOffsetY;
                GraphCanvas.Children.Add(line);
                xPos += xStep;
            }
        }

        /// <summary>
        /// get text to be drawn on canvas
        /// Source: https://social.msdn.microsoft.com/Forums/en-US/cb8e1e12-6ab4-43e4-8f4e-fb8437d857a4/drawing-graphics-text-to-a-wpf-canvas?forum=wpf
        /// </summary>
        /// <param name="xPos">x position to draw text</param>
        /// <param name="yPos">y position to draw text</param>
        /// <param name="text">text to write</param>
        /// <param name="colour">in which colour</param>
        private TextBlock GetText(double xPos, double yPos, string text, SolidColorBrush colour)
        {
            TextBlock textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.Foreground = colour;
            Canvas.SetLeft(textBlock, xPos);
            Canvas.SetTop(textBlock, yPos);
            return textBlock;
        }
    }
}

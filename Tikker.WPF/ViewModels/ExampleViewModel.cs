﻿using System;
using System.Collections.Generic;
using System.Text;
using Tikker.WPF.Commands;
using Tikker.WPF.States.Navigators;

namespace Tikker.WPF.ViewModels
{
    /// <summary>
    /// example viewmodel used in a presentation internally between de dev team.
    /// kept in the project as a reference.
    /// </summary>
    public class ExampleViewModel : ViewModel
    {
        private string _userInput { get; set; }
        /// <summary>
        /// user input from an input textbox
        /// </summary>
        public string UserInput
        {
            get
            {
                return _userInput;
            }
            set
            {
                _userInput = value;
                OnPropertyChanged(nameof(UserInput));
            }
        }

        private string _result { get; set; }
        /// <summary>
        /// result text
        /// </summary>
        public string Result
        {
            get
            {
                return _result;
            }
            set
            {
                _result = value;
                OnPropertyChanged(nameof(Result));
            }
        }


        public ExampleCommand ExampleCommand => new ExampleCommand(this);


        public ExampleViewModel(INavigator navigator) : base(navigator)
        {
            UserInput = "Hallo allemaal!";
        }

    }
}

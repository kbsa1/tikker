﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Tikker.WPF.States.Navigators;

namespace Tikker.WPF.ViewModels
{
    public interface IViewModel
    {
        event PropertyChangedEventHandler PropertyChanged;
        INavigator Navigator { get; set; }
    }
}

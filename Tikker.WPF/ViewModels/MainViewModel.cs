﻿using System;
using System.Collections.Generic;
using System.Text;
using Tikker.WPF.States.Navigators;

namespace Tikker.WPF.ViewModels
{
    /// <summary>
    /// global viewmodel
    /// </summary>
    public class MainViewModel : ViewModel
    {
        public MainViewModel() : this(new Navigator())
        {
        }

        public MainViewModel(INavigator navigator) : base(navigator)
        {
        }
    }
}

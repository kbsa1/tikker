﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Media;
using Tikker.Domain.Models;
using Tikker.Domain.Services;
using Tikker.EntityFramework;
using Tikker.EntityFramework.Services;
using Tikker.WPF.Converters;
using Tikker.WPF.States.Navigators;
using Color = System.Windows.Media.Color;

namespace Tikker.WPF.ViewModels
{
    public class BadgesViewModel : ViewModel
    {
        private IEnumerable<Badge> _badges;

        public IEnumerable<Badge> Badges
        {
            get { return _badges; }
            set
            {
                _badges = value;
                OnPropertyChanged(nameof(Badges));
            }
        }

        public BadgeCategory Category { get; }

        // Converts badge to background color
        public BadgeBackgroundConverter BadgeBackgroundConverter;

        public BadgesViewModel(INavigator navigator, BadgeCategory badgeCategory) : base(navigator)
        {
            Category = badgeCategory;
            LoadBadges();
        }

        /// <summary>
        /// Load badges of this category from the database
        /// </summary>
        private async void LoadBadges()
        {
            BadgeService dataService = new BadgeService(new TikkerDbContextFactory());
            Badges = await dataService.GetByCategory(Category);
        }
    }
}
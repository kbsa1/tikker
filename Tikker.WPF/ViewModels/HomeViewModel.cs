﻿using System.Windows.Input;
using Tikker.WPF.Commands;
using Tikker.WPF.States.Navigators;

namespace Tikker.WPF.ViewModels
{
    public class HomeViewModel : ViewModel
    {

        public ICommand LoadHomeCommand => new LoadHomeCommand(this);

        private string _avgWPM;
        public string AvgWPM
        {
            get
            {
                return _avgWPM;
            }
            set
            {
                _avgWPM = value;
                OnPropertyChanged(nameof(AvgWPM));
            }
        }


        private bool _statisticsButtonIsEnabled;

        /// <summary>
        /// True if the statistics button should be enabled
        /// </summary>
        public bool StatisticsButtonIsEnabled
        {
            get
            {
                return _statisticsButtonIsEnabled;
            }
            set
            {
                _statisticsButtonIsEnabled = value;
                OnPropertyChanged(nameof(StatisticsButtonIsEnabled));
            }
        }

        public HomeViewModel(INavigator navigator) : base(navigator)
        {
        }
    }
}

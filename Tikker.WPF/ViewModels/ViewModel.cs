﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Tikker.WPF.States.Navigators;

namespace Tikker.WPF.ViewModels
{
    /// <summary>
    /// base class for all viewmodels
    /// </summary>
    public abstract class ViewModel : IViewModel, INotifyPropertyChanged
    {
        /// <summary>
        /// event to call property changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// navigator used to change between views
        /// </summary>
        public INavigator Navigator { get; set; }

        public ViewModel(INavigator navigator)
        {
            Navigator = navigator;
        }

        /// <summary>
        /// method to call property changed event
        /// </summary>
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

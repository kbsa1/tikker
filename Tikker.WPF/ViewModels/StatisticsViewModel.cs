﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Tikker.Domain.Models;
using Tikker.WPF.Commands;
using Tikker.WPF.States.Navigators;

namespace Tikker.WPF.ViewModels
{
    public class StatisticsViewModel : ViewModel
    {
        private bool _loaded = false;
        /// <summary>
        /// if initialisation is completed
        /// </summary>
        public bool Loaded
        {
            get
            {
                return _loaded;
            }
            set
            {
                _loaded = value;
                OnPropertyChanged(nameof(Loaded));
            }
        }

        private string _difficultyCategory = null;
        /// <summary>
        /// Label for the current user's difficulty category.
        /// (Beginner, Average or Experienced)
        /// </summary>
        public string DifficultyCategory
        {
            get
            {
                return _difficultyCategory;
            }
            set
            {
                _difficultyCategory = value;
                OnPropertyChanged(nameof(DifficultyCategory));
            }
        }

        private string _avgWPM;
        /// <summary>
        /// Label for the average WPM of the user
        /// </summary>
        public string AvgWPM
        {
            get
            {
                return _avgWPM;
            }
            set
            {
                _avgWPM = value;
                OnPropertyChanged(nameof(AvgWPM));
            }
        }

        private string _avgAccuracy;
        /// <summary>
        /// label for the average accuracy
        /// </summary>
        public string AvgAccuracy
        {
            get
            {
                return _avgAccuracy;
            }
            set
            {
                _avgAccuracy = value;
                OnPropertyChanged(nameof(AvgAccuracy));
            }
        }

        private string _avgScore;
        /// <summary>
        /// label for the average score
        /// </summary>
        public string AvgScore
        {
            get
            {
                return _avgScore;
            }
            set
            {
                _avgScore = value;
                OnPropertyChanged(nameof(AvgScore));
            }

        }

        private List<Statistic> _statistics = null;
        /// <summary>
        /// a list of all statistics completed by the user
        /// </summary>
        public List<Statistic> Statistics
        {
            get
            {
                return _statistics;
            }
            set
            {
                _statistics = value;
                OnPropertyChanged(nameof(Statistics));
            }
        }

        /// <summary>
        /// fills all properties
        /// and sets the _loaded property to true once finished.
        /// Should be ran when the view is ready.
        /// </summary>
        public ICommand StatisticsLoadedCommand => new StatisticsLoadedCommand(this);
        
        public StatisticsViewModel(INavigator navigator) : base(navigator)
        {}
    }
}

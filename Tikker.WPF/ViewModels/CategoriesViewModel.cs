﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Tikker.Domain.Models;
using Tikker.Domain.Services;
using Tikker.EntityFramework;
using Tikker.EntityFramework.Services;
using Tikker.WPF.Commands;
using Tikker.WPF.States.Navigators;

namespace Tikker.WPF.ViewModels
{
    public class CategoriesViewModel : ViewModel
    {
        private IEnumerable<BadgeCategoryItem> _badgeCategories;

        public IEnumerable<BadgeCategoryItem> BadgeCategories
        {
            get { return _badgeCategories; }
            set
            {
                _badgeCategories = value;

                UpdateTotalBadgesAndTotalBadgesAchieved();

                OnPropertyChanged(nameof(BadgeCategories));
            }
        }

        private int _totalBadgesAchieved;

        public int TotalBadgesAchieved
        {
            get { return _totalBadgesAchieved; }
            set
            {
                _totalBadgesAchieved = value;
                OnPropertyChanged(nameof(TotalBadgesAchieved));
                OnPropertyChanged(nameof(TotalProgress));
            }
        }

        private int _totalBadges;

        public int TotalBadges
        {
            get { return _totalBadges; }
            set
            {
                _totalBadges = value;
                OnPropertyChanged(nameof(TotalBadges));
                OnPropertyChanged(nameof(TotalProgress));
            }
        }

        public string TotalProgress => $"Total amount of badges achieved: {TotalBadgesAchieved} / {TotalBadges}";

        private RelayCommand _loadCategoryCommand;

        public ICommand LoadCategoryCommand
        {
            get
            {
                if (_loadCategoryCommand == null)
                {
                    _loadCategoryCommand = new RelayCommand(ExecuteLoadCategoryCommand);
                }

                return _loadCategoryCommand;
            }
        }

        public CategoriesViewModel(INavigator navigator) : base(navigator)
        {
            LoadBadgeCategories();
        }

        /// <summary>
        /// Load all badge categories from the database
        /// </summary>
        private async void LoadBadgeCategories()
        {
            BadgeCategoryService dataService = new BadgeCategoryService(new TikkerDbContextFactory());
            IEnumerable<BadgeCategory> badgeCategories = await dataService.GetAll();
            List<BadgeCategoryItem> newCategories = new List<BadgeCategoryItem>();

            UserBadgesService userBadgesService = new UserBadgesService(new TikkerDbContextFactory());
            foreach (var category in badgeCategories)
            {
                int achieved = await userBadgesService.GetAchievedForCategory(category.Id);
                BadgeCategoryItem item = new BadgeCategoryItem {AmountAchieved = achieved, BadgeCategory = category};
                newCategories.Add(item);
            }

            BadgeCategories = newCategories;
        }

        /// <summary>
        /// Navigate to the badges screen
        /// </summary>
        /// <param name="sender">The clicked badge category</param>
        private void ExecuteLoadCategoryCommand(object sender)
        {
            Navigator.CurrentViewModel = new BadgesViewModel(Navigator, sender as BadgeCategory);
        }

        /// <summary>
        /// Update the total amount of badges and the total amount of badges achieved
        /// </summary>
        private void UpdateTotalBadgesAndTotalBadgesAchieved()
        {
            int totalAchieved = 0;
            int totalBadges = 0;

            foreach (var badgeCategory in BadgeCategories) // for every badge category
            {
                totalAchieved += badgeCategory.AmountAchieved; // add the achieved amount of this category to the total
                totalBadges += badgeCategory.BadgeCategory.Badges.Count; // add the badges amount of this category to the total
            }
            TotalBadgesAchieved = totalAchieved;
            TotalBadges = totalBadges;

        }
    }
}
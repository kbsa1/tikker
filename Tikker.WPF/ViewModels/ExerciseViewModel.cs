﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using SpeechLib;
using Tikker.Domain.Models;
using Tikker.Domain.Services;
using Tikker.EntityFramework;
using Tikker.EntityFramework.Services;
using Tikker.WPF.Commands;
using Tikker.WPF.States.Navigators;
using Tikker.WPF.Views;
using Popup = Tikker.WPF.Views.Popup;

namespace Tikker.WPF.ViewModels
{
    public class ExerciseViewModel : ViewModel
    {
        public ICommand NextExerciseCommand { get; private set; }
        public ICommand SameExerciseCommand => new SameExerciseCommand(this);

        public ICommand ReloadViewModelCommand { get; private set; }
        public ICommand ReloadViewModelSameExerciseCommand { get; private set; }



        private IStatisticsService _statisticsService = new StatisticsService(new TikkerDbContextFactory());
        private GenericDataService<Statistic> _genericDataService = new GenericDataService<Statistic>(new TikkerDbContextFactory());

        /// <summary>
        /// Amount of seconds before TTS reads out the current word again when not typing
        /// </summary>
        private const int TtsAutospeakInterval = 2;

        /// <summary>
        /// Tracks if the user has started typing in the input field or not
        /// </summary>
        private bool _startedTyping = false;

        /// <summary>
        /// Used for badge db connection
        /// </summary>
        int ExercisesTotalCount;

        List<Badge> badges;
        List<Exercise> exercises;
        List<UserBadge> AllUserBadges;

        GenericEntityService<UserBadge> UserBadgesService = new GenericEntityService<UserBadge>(new TikkerDbContextFactory());
        BadgeService BadgesService = new BadgeService(new TikkerDbContextFactory());

        ExerciseService ExerciseService = new ExerciseService(new TikkerDbContextFactory());

        
        private bool _isOpen;

        /// <summary>
        /// Tracks whether the badge popup needs to be visible
        /// </summary>
        public bool IsOpen
        {
            get { return _isOpen; }
            set
            {
                if (_isOpen == value) return;
                _isOpen = value;
                OnPropertyChanged("IsOpen");
            }
        }

        private string _exerciseText;

        /// <summary>
        /// Contains the full text that the user has to type
        /// </summary>
        public string ExerciseText
        {
            get => _exerciseText;
            set
            {
                _exerciseText = value;
                // fill linkedlist
                Words.Clear();
                string[] words = value.Split(' ');
                for (int i = 0; i < words.Length; ++i)
                {
                    // all words but the last require a space
                    string suffix = (i < words.Length - 1) ? " " : "";
                    Words.AddLast(words[i] + suffix);
                }

                // reset currentword
                CurrentWord = Words.First;
                OnPropertyChanged(nameof(ExerciseText));
            }
        }

        /// <summary>
        /// Linkedlist of all of the words that the user has to type (contains spaces at the end of each word)
        /// </summary>
        public LinkedList<string> Words = new LinkedList<string>();

        /// <summary>
        /// Calculates the total amount of characters in the text
        /// </summary>
        /// <returns></returns>
        public int CalculateTotalCharacters()
        {
            int totalCharacters = 0;
            foreach (string word in Words)
            {
                totalCharacters += word.Length;
            }

            return totalCharacters;
        }

        private LinkedListNode<string> _currentWord = null;

        /// <summary>
        /// Contains the Current word the user is typing
        /// </summary>
        public LinkedListNode<string> CurrentWord
        {
            get => _currentWord;
            set
            {
                _currentWord = value;
                OnPropertyChanged(nameof(CurrentWord));
            }
        }

        /// <summary>
        /// The amount of letters that have already been correctly input by the user.
        /// If the user performs a backspace the correct doesn't go down.
        /// Used to prevent cheating WPM.
        /// </summary>
        private int _correctCharactersInWord;

        /// <summary>
        /// The amount of letters the user has written incorrectly
        /// </summary>
        public int IncorrectCharactersInWord;

        private int _correctlyTypedCharacters;

        /// <summary>
        /// The amount of correct characters the user has already typed correctly in total
        /// </summary>
        public int CorrectlyTypedCharacters
        {
            get => _correctlyTypedCharacters;
            set => _correctlyTypedCharacters = value;
        }

        private string _exerciseInput = "";

        /// <summary>
        /// Contains the input the user has typed in the input field
        /// </summary>
        public string ExerciseInput
        {
            get => _exerciseInput;
            set
            {
                if (!_startedTyping) // if the user hasn't typed before
                {
                    SetTimer(); // start the timer
                    _startedTyping = true;
                }

                _lastTimeSpokeOrTyped = DateTime.Now;
                _exerciseInput = value;
                CheckInput(_exerciseInput);
                OnPropertyChanged(nameof(ExerciseInput));
            }
        }

        private int _liveWpm;

        /// <summary>
        /// Contains the current WPM value (while the user is typing)
        /// </summary>
        public int LiveWpm
        {
            get => _liveWpm;
            set
            {
                _liveWpm = value;
                OnPropertyChanged(nameof(LiveWpm));
            }
        }

        private string _keyToType;

        /// <summary>
        /// Contains the key the user has to type next
        /// </summary>
        public string KeyToType
        {
            get => _keyToType;
            set
            {
                _keyToType = value;
                OnPropertyChanged(nameof(KeyToType));
            }
        }

        private bool _isFinished;

        public bool IsFinished
        {
            get => _isFinished;
            set
            {
                _isFinished = value;
                OnPropertyChanged(nameof(IsFinished));
            }
        }

        /// <summary>
        /// Contains the Key to type, but case sensitive, so it can be used for the visual keyboard
        /// </summary>
        public string CaseSensitiveKeyToType { get; set; }

        /// <summary>
        /// Contains the path to the current text to speech image, based on if TTS is enabled or not.
        /// </summary>
        public string TTSImagePath => TTSEnabled ? "../Views/Icons/tts-enabled.png" : "../Views/Icons/tts-disabled.png";
        
        /// <summary>
        /// Contains the path to the current keyboard collapse icon image, based on if keyboard is collapsed or not.
        /// </summary>
        
        public string KeyboardCollapseButtonImagePath => KeyboardIsCollapsed ? "../Views/Icons/keyboard_close_collapsed.png" : "../Views/Icons/keyboard_close.png";

        private bool _ttsEnabled;

        /// <summary>
        /// Tracks if text to speech is enabled by the user
        /// </summary>
        public bool TTSEnabled
        {
            get => _ttsEnabled;
            set
            {
                _ttsEnabled = value;
                OnPropertyChanged(nameof(TTSImagePath));
            }
        }

        /// <summary>
        /// Saves if the keyboard is collapsed or not (for the next exercise)
        /// </summary>
        private bool _keyboardCollapsedBeforeFinish;

        private bool _keyboardIsCollapsed;

        /// <summary>
        /// Tracks if the keyboard is collapsed or not
        /// </summary>
        public bool KeyboardIsCollapsed
        {
            get => _keyboardIsCollapsed;
            set
            {
                _keyboardIsCollapsed = value;
                OnPropertyChanged(nameof(KeyboardCollapseButtonImagePath));
                OnPropertyChanged(nameof(KeyboardIsCollapsed));
            }
        }

        /// <summary>
        /// Timer that is used for time-based functions
        /// </summary>
        private Timer _timer;

        /// <summary>
        /// Stores the moment when the user started typing the exercise
        /// </summary>
        public DateTime TimeStarted;

        /// <summary>
        /// Used for text to speech
        /// </summary>
        private readonly SpVoice _voice;

        /// <summary>
        /// Stores the last time TTS spoke or the user typed something
        /// </summary>
        private DateTime _lastTimeSpokeOrTyped;

        /// <summary>
        /// Creates a list with words that are typed incorrectly
        /// </summary>
        public List<LinkedListNode<string>> IncorrectWordsList = new List<LinkedListNode<string>>();

        /// <summary>
        /// Creates a list with words that are typed incorrectly, with special characters removed
        /// </summary>
        public List<LinkedListNode<string>> IncorrectWordsListWithOutSpecialCharacters = new List<LinkedListNode<string>>();

        private string _incorrectWords;

        /// <summary>
        /// String that contains all of the words that the user has typed incorrectly, seperated by line breaks.
        /// </summary>
        public string IncorrectWords
        {
            get
            {
                _incorrectWords = "";
                for (int i = 0; i < IncorrectWordsListWithOutSpecialCharacters.Count; i++)
                {
                    _incorrectWords = $"{_incorrectWords} {IncorrectWordsListWithOutSpecialCharacters[i].Value} \n";
                }

                return _incorrectWords;
            }
            set
            {
                _incorrectWords = value;
                OnPropertyChanged(nameof(IncorrectWords));
            }
        }

        private int _sumWrongLetters;

        /// <summary>
        /// Contains the amount of the letters the user has typed incorrectly
        /// </summary>>
        public int SumWrongLetters
        {
            get => _sumWrongLetters;

            set => _sumWrongLetters = value;
        }


        private Statistic _statistic;

        /// <summary>
        /// Contains the statistic for the current exercise
        /// </summary>
        public Statistic Statistic
        {
            get => _statistic;
            set
            {
                _statistic = value;
                OnPropertyChanged(nameof(Statistic));
            }
        }

        private PopupManager _popupManager;
        public ObservableCollection<Popup> Popups => _popupManager.Popups;

        /// <summary>
        /// Constructor for ExerciseViewModel
        /// </summary>
        /// <param name="navigator"></param>
        public ExerciseViewModel(INavigator navigator) : base(navigator)
        {
            _voice = new SpVoice();
            _popupManager = new PopupManager();
            ReloadViewModelCommand = new RelayCommand(ReloadViewModelNextExercise);
            ReloadViewModelSameExerciseCommand = new RelayCommand(ReloadViewModelSameExercise);
            NextExerciseCommand = new NextExerciseCommand(
                this,
                new ExerciseService(new TikkerDbContextFactory()),
                new StatisticsService(new TikkerDbContextFactory())
            );
        }

        /// <summary>
        /// Compare the user input with the current word and act accordingly
        /// </summary>
        /// <param name="input">text the user input</param>
        private void CheckInput(string input)
        {
            if (CurrentWord == null)
            {
                return;
            }

            IncorrectCharactersInWord = 0;
            for (int i = 0; i < input.Length; ++i)
            {
                // if the letter is correct
                if (i < CurrentWord.Value.Length && input[i] == CurrentWord.Value[i])
                {
                    // only count characters that have not been entered correctly before
                    // to avoid cheating of WPM
                    if (i > _correctCharactersInWord)
                    {
                        _correctCharactersInWord += 1;
                        CorrectlyTypedCharacters += 1;
                    }
                }
                // count incorrectly entered letters
                else
                {
                    IncorrectCharactersInWord += 1;
                }
            }

            // if word is spelled correctly
            // only go to next word when ends with space, except for last word
            if (input == CurrentWord.Value)
            {
                _correctCharactersInWord = 0;
                // if text finished
                if (CurrentWord.Next == null)
                {
                    OnExerciseFinished();
                }
                // if still typing exercise                                                                                             
                else
                {
                    _correctCharactersInWord = 0;
                    CurrentWord = CurrentWord.Next;
                    _exerciseInput = "";

                    // say next word
                    if (TTSEnabled)
                    {
                        _lastTimeSpokeOrTyped = DateTime.Now;
                        _voice.Speak(CurrentWord.Value,
                            SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFPurgeBeforeSpeak);
                    }
                }
            }
        }

        private int _highscore;

        /// <summary>
        /// Stores the user's highscore
        /// </summary>
        public int Highscore
        {
            get => _highscore;
            set
            {
                _highscore = value;
                OnPropertyChanged(nameof(Highscore));
            }
        }

        /// <summary>
        /// Gets the exerciseId
        /// </summary>
        public int ExerciseId;

        /// <summary>
        /// Get all badgesamounts from the database
        /// </summary>
        private async Task GetBadgesAmounts()
        {
            badges = new List<Badge>(await BadgesService.GetAll());
        }
        /// <summary>
        /// Get the total exercises from the database
        /// </summary>
        private async Task GetTotalExercises()
        {
            exercises = new List<Exercise>(await ExerciseService.GetAll());
            ExercisesTotalCount = exercises.Count;
        }

        /// <summary>
        /// Get user badges from the database
        /// </summary>
        private async Task GetAllUserBadges()
        {
            AllUserBadges = new List<UserBadge>(await UserBadgesService.GetAll());
        }

        /// <summary>
        /// Returns true if the user has a certain badge
        /// </summary>
        /// <param name="badge"></param>
        /// <returns></returns>
        private bool UserHasBadge(Badge badge)
        {
            bool cameAcrossBadge = false;

            foreach (UserBadge userBadge in AllUserBadges)
            {
                if (userBadge.BadgeId == badge.Id)
                {
                    cameAcrossBadge = true;
                }
            }

            return cameAcrossBadge;
        }

        /// <summary>
        /// Contains what to do when an exercise is finished
        /// </summary>
        private async void OnExerciseFinished()
        {
            // The keyboard should collapse when an exercise is finished
            _keyboardCollapsedBeforeFinish = KeyboardIsCollapsed;
            KeyboardIsCollapsed = true;
            
            var time = DateTime.Now.Subtract(TimeStarted);
            Statistic statistic = new Statistic();
            statistic.WPM = _liveWpm;
            statistic.Time = time;
            statistic.Accuracy = Statistic.CalculateAccuracy(_sumWrongLetters, CalculateTotalCharacters());

            //score is being calculated
            statistic.Score =
                Statistic.CalculateScore(400, CalculateTotalCharacters(), statistic.Accuracy, statistic.WPM);
            IncorrectWords = $"{IncorrectWords} ";
            statistic.TakenOn = DateTime.Now;
            statistic.ExerciseId = ExerciseId;
            await _statisticsService.Create(statistic);

            // get highscore from database
            Statistic highscoreStatistic = await _statisticsService.GetHighScore(ExerciseId);
            Highscore = (highscoreStatistic != null) ? highscoreStatistic.Score : 0;
            Statistic = statistic;
            IsFinished = true;

            //WPM BADGES
            int WPM = statistic.WPM;
            int score = statistic.Score;
            int accuracy = statistic.Accuracy;
            int accuracyStreak = -1;
            await GetBadgesAmounts();
            await GetTotalExercises();
            
            // Check accuracy streak. We only need to check the streak if current accuracy is 100%.
            if (accuracy == 100)
            {
                accuracyStreak = await _statisticsService.GetAccuracyStreak();
                if (accuracyStreak >= 2)
                {
                    _popupManager.ShowPopup("Accuracy streak!", $"You have completed {accuracyStreak} texts without mistakes",
                        TimeSpan.FromSeconds(5), "/Views/Icons/target.png");
                }
            }

            // Warning badges, when almost reached
            if (score == Highscore)
            {
                _popupManager.ShowPopup("New highscore!", $"Congrats, {score} is your new highscore!",
                    TimeSpan.FromSeconds(5), "/Views/Icons/trophy.png");
            }
            
            else if (Highscore - score <= 15)
            {
                _popupManager.ShowPopup("Almost!", $"You were {Highscore - score} points away from beating your own highscore!",
                    TimeSpan.FromSeconds(5), "/Views/Icons/trophy.png");
            }
            

            foreach (Badge badge in badges)
            {
                int amount = badge.Amount;

                await GetAllUserBadges();

                bool userHasBadge = UserHasBadge(badge);

                if (userHasBadge == false)
                {
                    //category 1
                    if (badge.BadgeCategory.Id == 1)
                    {
                        if (ExercisesTotalCount >= amount)
                        {
                            UserBadge newUserBadge = new UserBadge() { UserId = 1, BadgeId = badge.Id };
                            await UserBadgesService.Create(newUserBadge);

                            _popupManager.ShowPopup("New badge!", $"You've earned a badge for doing {amount} exercises!",
                                TimeSpan.FromSeconds(5), badge.BadgeCategory.Icon);
                        }
                        
                        else if (amount - ExercisesTotalCount <= 10)
                        {
                            _popupManager.ShowPopup("So close!", $"Complete {amount - ExercisesTotalCount} more exercises to earn a new badge",
                                TimeSpan.FromSeconds(5), badge.BadgeCategory.Icon);
                        }
                    }

                    //category 2
                    if (badge.BadgeCategory.Id == 2)
                    {
                        if (WPM >= amount)
                        {
                            UserBadge newUserBadge = new UserBadge() { UserId = 1, BadgeId = badge.Id };
                            await UserBadgesService.Create(newUserBadge);

                            _popupManager.ShowPopup("New badge!", $"You've earned a badge for reaching a WPM of {amount}",
                                TimeSpan.FromSeconds(5), badge.BadgeCategory.Icon);
                        }
                        
                        else if (amount-WPM <= 10 && amount-WPM > 0)
                        {
                            _popupManager.ShowPopup("You were so close", $"You needed {amount-WPM} WPM more to earn a new badge",
                                TimeSpan.FromSeconds(5), badge.BadgeCategory.Icon);
                        }
                    }

                    //category 3
                    if (badge.BadgeCategory.Id == 3)
                    {
                        if (accuracyStreak >= amount)
                        {
                            UserBadge newUserBadge = new UserBadge() { UserId = 1, BadgeId = badge.Id };
                            await UserBadgesService.Create(newUserBadge);

                            _popupManager.ShowPopup("New badge!", $"You have completed {accuracyStreak} texts without mistakes",
                                TimeSpan.FromSeconds(5), badge.BadgeCategory.Icon);
                        }
                    }

                    //category 4
                    if (badge.BadgeCategory.Id == 4)
                    {
                        if (Highscore >= amount)
                        {
                            UserBadge newUserBadge = new UserBadge() { UserId = 1, BadgeId = badge.Id };
                            await UserBadgesService.Create(newUserBadge);

                            _popupManager.ShowPopup("New badge!!", $"You've earned a badge for reaching a highscore of {amount}!",
                                TimeSpan.FromSeconds(5), badge.BadgeCategory.Icon);
                        }
                    }

                    //category 5
                    if (badge.BadgeCategory.Id == 5)
                    {
                        if (score >= amount)
                        {
                            UserBadge newUserBadge = new UserBadge() { UserId = 1, BadgeId = badge.Id };
                            await UserBadgesService.Create(newUserBadge);


                            _popupManager.ShowPopup("New badge!", $"You've earned a badge for reaching a score of {amount}!",
                                TimeSpan.FromSeconds(5), badge.BadgeCategory.Icon);
                        }
                        
                        else if (amount - score <= 10)
                        {
                            _popupManager.ShowPopup("So close!", $"Get a score of {amount} to earn a new badge",
                                TimeSpan.FromSeconds(5), badge.BadgeCategory.Icon);
                        }
                    }

                    //category 6
                    if (badge.BadgeCategory.Id == 6)
                    {
                        if (AllUserBadges.Count >= amount)
                        {
                            UserBadge newUserBadge = new UserBadge() { UserId = 1, BadgeId = badge.Id };
                            await UserBadgesService.Create(newUserBadge);


                            _popupManager.ShowPopup("New badge!", $"You've earned a badge for making {amount} exercises!",
                                TimeSpan.FromSeconds(5), badge.BadgeCategory.Icon);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Reloads the viewmodel and starts a new exercise
        /// </summary>
        /// <param name="parameter"></param>
        public void ReloadViewModelNextExercise(object parameter)
        {
            ResetVariables();
            NextExerciseCommand.Execute(null);
        }

        /// <summary>
        /// Reloads the viewmodel and starts the same exercise again
        /// </summary>
        /// <param name="parameter"></param>
        public void ReloadViewModelSameExercise(object parameter)
        {
            ResetVariables();
            SameExerciseCommand.Execute(null);
        }

        /// <summary>
        /// Resets all variables to their default value
        /// </summary>
        public void ResetVariables()
        {
            _startedTyping = false;
            _correctCharactersInWord = 0;
            IncorrectCharactersInWord = 0;
            CorrectlyTypedCharacters = 0;
            ExerciseInput = "";
            LiveWpm = 0;
            SumWrongLetters = 0;
            IncorrectWords = null;

            IncorrectWordsList = new List<LinkedListNode<string>>();
            IsFinished = false;
            KeyboardIsCollapsed = _keyboardCollapsedBeforeFinish;
        }

        /// <summary>
        /// Starts the timer (the timer is used in other functions)
        /// </summary>
        private void SetTimer()
        {
            // Create a timer with a 0.5 second interval.
            _timer = new System.Timers.Timer(500);
            // Hook up the Elapsed event for the timer. 
            _timer.Elapsed += OnTimedEvent;
            _timer.AutoReset = true;
            _timer.Enabled = true;
            TimeStarted = DateTime.Now;
            Debug.WriteLine("Timer started");
        }

        /// <summary>
        /// StringBuilder that helps to build the string for which key to press
        /// </summary>
        private StringBuilder _keyToPress;

        /// <summary>
        /// Takes the raw input and formats it as a KeyToPress string
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public string Key(string Input)
        {
            _keyToPress = new StringBuilder();
            Input = Input.ToUpper();
            _keyToPress.Append("Key_");

            string input = Input;

            switch (input)
            {
                case " ":
                    _keyToPress.Append("Space");
                    break;
                case "1":
                    _keyToPress.Append("D1");
                    break;
                case "2":
                    _keyToPress.Append("D2");
                    break;
                case "3":
                    _keyToPress.Append("D3");
                    break;
                case "4":
                    _keyToPress.Append("D4");
                    break;
                case "5":
                    _keyToPress.Append("D5");
                    break;
                case "6":
                    _keyToPress.Append("D6");
                    break;
                case "7":
                    _keyToPress.Append("D7");
                    break;
                case "8":
                    _keyToPress.Append("D8");
                    break;
                case "9":
                    _keyToPress.Append("D9");
                    break;
                case "0":
                    _keyToPress.Append("D0");
                    break;
                case ",":
                    _keyToPress.Append("OemComma");
                    break;
                case ".":
                    _keyToPress.Append("OemPeriod");
                    break;
                case "'":
                    _keyToPress.Append("OemQuotes");
                    break;
                case "\"":
                    _keyToPress.Append("OemQuotes");
                    break;
                case "-":
                    _keyToPress.Append("OemMinus");
                    break;
                case "+":
                    _keyToPress.Append("OemPlus");
                    break;
                case "?":
                    _keyToPress.Append("OemQuestion");
                    break;
                case "!":
                    _keyToPress.Append("D1");
                    break;
                default:
                    _keyToPress.Append(Input);
                    break;
            }

            return _keyToPress.ToString();
        }


        /// <summary>
        /// Executed every timed event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (!IsFinished)
            {
                var timeSpan = DateTime.Now.Subtract(TimeStarted);
                LiveWpm = Statistic.CalculateWPM(timeSpan.TotalMinutes,
                    CorrectlyTypedCharacters); // calculate wpm and update variable

                if (TTSEnabled)
                {
                    TimeSpan sinceLastTyped = DateTime.Now.Subtract(_lastTimeSpokeOrTyped);
                    if (sinceLastTyped.Seconds >= TtsAutospeakInterval)
                    {
                        _lastTimeSpokeOrTyped = DateTime.Now;
                        _voice.Speak(CurrentWord.Value,
                            SpeechVoiceSpeakFlags.SVSFlagsAsync | SpeechVoiceSpeakFlags.SVSFPurgeBeforeSpeak);
                    }
                }
            }

            _popupManager.RemovePopups();
        }
    }
}
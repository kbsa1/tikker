﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Tikker.WPF.Commands;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.States.Navigators
{
    /// <summary>
    /// The navigator handles the switching of views.
    /// And holds the instance of the current active viewmodel.
    /// </summary>
    public class Navigator : INavigator, INotifyPropertyChanged
    {
        /// <summary>
        /// view to show on startup
        /// </summary>
        const View STARTUP_VIEW = View.Home;

        private IViewModel _currentViewModel { get; set; }
        /// <summary>
        /// The currently active viewmodel
        /// </summary>
        public IViewModel CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                // add to history
                if(_currentViewModel != null)
                {
                    _viewModelHistory.Push(_currentViewModel);
                }
                // update
                _currentViewModel = value;
                OnPropertyChanged(nameof(CurrentViewModel));
            }
        }

        /// <summary>
        /// Command to change the current viewmodel.
        /// </summary>
        /// <example>
        /// Example of usage in a view:
        /// <code>
        /// <Button Command="{Binding Navigator.UpdateCurrentViewModelCommand}" CommandParameter="{x:Static nav:View.Exercise} />
        /// </code>
        /// </example>
        public ICommand UpdateCurrentViewModelCommand { get; private set; }

        /// <summary>
        /// Command to go back to the previously visited Viewmodel. Uses Back() method.
        /// </summary>
        /// <example>
        /// Example of usage in a view:
        /// <code>
        /// <Button Command="{Binding Navigator.BackCommand}" />
        /// </code>
        /// </example>
        public ICommand BackCommand { get; private set; }

        ///// <summary>
        ///// here we cache old viewmodels so they can be reloaded with the same data
        ///// </summary>
        //protected Dictionary<Type, IViewModel> _cachedViewModels = new Dictionary<Type, IViewModel>();

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// a stack of the history of visited viewmodels.
        /// when a back button is pressed, is removed from viewmodelhistory
        /// </summary>
        protected Stack<IViewModel> _viewModelHistory = new Stack<IViewModel>();

        public Navigator()
        {
            // initialise relaycommands
            UpdateCurrentViewModelCommand = new RelayCommand(ChangeCurrentViewModel, CanChangeCurrentViewModel);
            BackCommand = new RelayCommand(OnBackCommand, CanGoBackCommand);

            // setup default view
            SetActiveFromView(STARTUP_VIEW);
        }

        /// <summary>
        /// change active viewmodel from view
        /// </summary>
        /// <param name="view"></param>
        public void SetActiveFromView(View view)
        {
            switch (view)
            {
                case View.Home:
                    CurrentViewModel = new HomeViewModel(this);
                    break;
                case View.Exercise:
                    //SetActiveFromCache<ExerciseViewModel>();
                    CurrentViewModel = new ExerciseViewModel(this);
                    break;
                case View.Statistics:
                    CurrentViewModel = new StatisticsViewModel(this);
                    break;
                case View.Example:
                    CurrentViewModel = new ExampleViewModel(this);
                    break;
                case View.Categories:
                    CurrentViewModel = new CategoriesViewModel(this);
                    break;
            }
        }

        /// <summary>
        /// go back to the previous viewmodel
        /// </summary>
        /// <returns>new current viewmodel or null</returns>
        public IViewModel Back()
        {
            if(_viewModelHistory.Count > 0)
            {
                _currentViewModel = _viewModelHistory.Pop();
                OnPropertyChanged(nameof(CurrentViewModel));
                return CurrentViewModel;
            }
            return null;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// if the viewmodel can be changed using the given parameter.
        /// Used for the UpdateCurrentViewModelCommand
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanChangeCurrentViewModel(object parameter)
        {
            return parameter is View;
        }

        /// <summary>
        /// changes viewmodel using the given parameter.
        /// Used for the UpdateCurrentViewModelCommand
        /// </summary>
        /// <param name="parameter"></param>
        public void ChangeCurrentViewModel(object parameter)
        {
            if (parameter is View)
            {
                View view = (View)parameter;
                SetActiveFromView(view);
            }
        }

        /// <summary>
        /// if its possible to go back in history.
        /// Returns true if there's anything in the history.
        /// </summary>
        /// <returns>if back can be returned</returns>
        public bool CanGoBackCommand(object parameter)
        {
            return _viewModelHistory.Count > 0;
        }

        /// <summary>
        /// run the Back() method for the command
        /// </summary>
        /// <param name="parameter"></param>
        public void OnBackCommand(object parameter)
        {
            Back();
        }

        ///// <summary>
        ///// set a cached viewmodel active. if it isn't cached a new one will be created.
        ///// </summary>
        ///// <typeparam name="T">which viewmodel to set active</typeparam>
        ///// <returns>the now active viewmodel</returns>
        //public IViewModel SetActiveFromCache<T>() where T : IViewModel
        //{
        //    Type type = typeof(T);
        //    // get from cache if already exists
        //    if (_cachedViewModels.ContainsKey(type))
        //    {
        //        CurrentViewModel = _cachedViewModels[type];
        //    }
        //    // else create a new one and store in cache
        //    else
        //    {
        //        CurrentViewModel = default(T);
        //        CurrentViewModel.Navigator = this;
        //        _cachedViewModels.Add(type, CurrentViewModel);
        //    }

        //    return CurrentViewModel;
        //}
    }
}

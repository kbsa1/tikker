﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Tikker.WPF.Commands;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.States.Navigators
{
    /// <summary>
    /// navigatable views
    /// </summary>
    public enum View
    {
        Home,
        Exercise,
        Example,
        Statistics,
        Badges,
        Categories
    }

    public interface INavigator
    {
        /// <summary>
        /// current active viewmodel
        /// </summary>
        IViewModel CurrentViewModel { get; set; }
        /// <summary>
        /// command to change active viewmodel
        /// </summary>
        ICommand UpdateCurrentViewModelCommand { get; }
        /// <summary>
        /// Command to call Back()
        /// </summary>
        ICommand BackCommand { get; }

        /// <summary>
        /// set active viewmodel from view
        /// </summary>
        /// <param name="view">view to show</param>
        void SetActiveFromView(View view);

        /// <summary>
        /// go back one viewmodel
        /// </summary>
        /// <returns>new current viewmodel or null</returns>
        public IViewModel Back();
    }
}

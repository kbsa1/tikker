using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using Microsoft.IdentityModel.Protocols;

namespace Tikker.EntityFramework
{
    /// <summary>
    /// Handles the creation of database context
    /// </summary>
    public class TikkerDbContextFactory : IDesignTimeDbContextFactory<TikkerDbContext>
    {
        /// <summary>
        /// create a database connection with a connection to the configured database.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public TikkerDbContext CreateDbContext(string[] args = null)
        {
            var options = new DbContextOptionsBuilder<TikkerDbContext>();

            // get connection string and use it to connect to the database
            string connectionString = GetConnectionString();
            if(connectionString != null)
            {
                options.UseSqlServer(connectionString);
            }

            return new TikkerDbContext(options.Options);
        }

        /// <summary>
        /// get a connection string to the database
        /// </summary>
        /// <returns></returns>
        private string GetConnectionString()
        {
            // try getting from the configuration manager
            var connection = ConfigurationManager.ConnectionStrings["TikkerDatabase"];
            if (connection != null && !string.IsNullOrWhiteSpace(connection.ConnectionString))
            {
                return connection.ConnectionString;
            }

            // if cant get from the config manager, get from the app config file itself
            if (connection == null)
            {
                // NOTE: EF 3.1 design tools no longer initializes ConfigurationManager  
                // so we have to load it manually for Add-Migration
                // https://github.com/dotnet/efcore/issues/19760
                connection = GetFromAppConfig();

                if (connection != null)
                {
                    return connection.ConnectionString;
                }
            }

            return "";
        }

        /// <summary>
        /// get connection string manually from app config file
        /// </summary>
        /// <returns></returns>
        private ConnectionStringSettings GetFromAppConfig()
        {
            // get app config file
            string directory = Environment.CurrentDirectory;
            string filePath = Path.Combine(directory, "../Tikker.WPF/App.config");

            if (File.Exists(filePath))
            {
                // get connection strings from app config file
                var map = new ExeConfigurationFileMap {ExeConfigFilename = filePath};
                var config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                var connection = config.ConnectionStrings.ConnectionStrings["TikkerDatabase"];
                if (connection != null)
                {
                    return connection;
                }
            }

            return null;
        }
    }
}

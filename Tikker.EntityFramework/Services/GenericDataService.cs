﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tikker.Domain.Models;
using Tikker.Domain.Services;

namespace Tikker.EntityFramework.Services
{
    /// <summary>
    /// Represents a dataservice for a given model.
    /// A model is an entity in the database with an ID.
    /// This service extends functionality for entities with IDs
    /// by providing methods like Get() which can be gotten with an ID.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericDataService<T> : GenericEntityService<T>, IDataService<T> where T : DomainObject
    {
        public GenericDataService(TikkerDbContextFactory contextFactory) : base(contextFactory)
        {
        }

        public async Task<T> Get(int id)
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    T entity = await context.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
                    return entity;
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return null;
            }
        }

        public async Task<T> Update(int id, T entity)
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    entity.Id = id;
                    context.Set<T>().Update(entity);
                    await context.SaveChangesAsync();

                    return entity;
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return entity;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    T entity = await context.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
                    context.Set<T>().Remove(entity);
                    await context.SaveChangesAsync();

                    return true;
                }
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tikker.Domain.Models;
using Tikker.Domain.Services;

namespace Tikker.EntityFramework.Services
{
    public class StatisticsService : GenericDataService<Statistic>, IStatisticsService
    {
        public StatisticsService(TikkerDbContextFactory contextFactory) : base(contextFactory)
        {
        }

        /// <summary>
        /// Get the highscore for a given exercise
        /// </summary>
        /// <param name="exerciseId">id of the exercise to get the highscore for</param>
        /// <returns></returns>
        public async Task<Statistic> GetHighScore(int exerciseId)
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    return await context.Set<Statistic>()
                        .Where(s => s.Exercise.Id == exerciseId)
                        .Distinct()
                        .OrderByDescending(s => s.Score)
                        .FirstOrDefaultAsync();
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return null;
            }
        }

        /// <summary>
        /// Get the highscore for a given exercise
        /// </summary>
        /// <param name="statistic">the exercise to get the highscore for</param>
        /// <returns></returns>
        public async Task<Statistic> GetHighScore(Exercise exercise)
        {
            return await GetHighScore(exercise.Id);
        }

        /// <summary>
        /// get the average score
        /// </summary>
        /// <returns>average score or -1 if no score</returns>
        public async Task<int> GetAverageScore()
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    DbSet<Statistic> statistics = context.Set<Statistic>();
                    if (statistics.Count() == 0)
                    {
                        return -1;
                    }

                    double average = await statistics.AverageAsync(s => s.Score);
                    return (int)average;
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return -1;
            }
        }

        /// <summary>
        /// get difficulty category based on average score
        /// </summary>
        /// <returns>difficulty category</returns>
        public async Task<DifficultyCategory> GetDifficultyCategory()
        {
            int score = await GetAverageScore();
            // beginner = 0L-600L
            if (score <= 40)
            {
                return DifficultyCategory.Beginner;
            }
            // average = 600L-1200L
            else if (score <= 80)
            {
                return DifficultyCategory.Average;
            }
            // experienced = 1200L+
            else
            {
                return DifficultyCategory.Experienced;
            }
        }

        /// <summary>
        /// Get the current amount of 100% accuracy in a row
        /// </summary>
        /// <returns>the current 100% accuracy streak</returns>
        public async Task<int> GetAccuracyStreak()
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    List<Statistic> list = await context.Statistics
                         .OrderByDescending(statistic => statistic.TakenOn).ToListAsync();
                    return list.TakeWhile(statistic => statistic.Accuracy == 100).Count();
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return 0;
            }
        }
    }
}

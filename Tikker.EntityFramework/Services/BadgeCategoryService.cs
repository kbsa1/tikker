﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tikker.Domain.Models;
using Tikker.Domain.Services;

namespace Tikker.EntityFramework.Services
{
    public class BadgeCategoryService : GenericDataService<BadgeCategory>, IBadgeCategoryService
    {
        public BadgeCategoryService(TikkerDbContextFactory contextFactory) : base(contextFactory)
        {
        }

        public async Task<IEnumerable<BadgeCategory>> GetAll()
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    ICollection<BadgeCategory> entities = await context.BadgeCategories
                        .Include(badgeCategory => badgeCategory.Badges)
                        .ToListAsync();
                    return entities;
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return new List<BadgeCategory>();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tikker.Domain.Models;
using Tikker.Domain.Services;
using Tikker.EntityFramework;
using Tikker.EntityFramework.Services;

namespace Tikker.EntityFramework.Services
{
    public class ExerciseService : GenericDataService<Exercise>, IExerciseService
    {


        public ExerciseService(TikkerDbContextFactory contextFactory) : base(contextFactory)
        {
        }

        /// <summary>
        /// Get a list of randomly ordered exercises.
        /// </summary>
        /// <returns>A list of random exercises</returns>
        public async Task<IEnumerable<Exercise>> GetAllShuffled()
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    return await context.Exercises
                        .OrderBy(item => Guid.NewGuid())
                        .ToListAsync();
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return new List<Exercise>();
            }
        }

        /// <summary>
        /// Get a list of randomly ordered exercises within a specific difficulty.
        /// Higher or equal to min, and lower or equal to maxl.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns>A list of random exercises</returns>
        public async Task<IEnumerable<Exercise>> GetShuffledWithinDifficulty(int min, int max)
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    return await context.Exercises
                        .Where(e => e.Difficulty >= min)
                        .Where(e => e.Difficulty <= max)
                        .OrderBy(item => Guid.NewGuid())
                        .ToListAsync();
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return new List<Exercise>();
            }
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tikker.Domain.Models;
using Tikker.Domain.Services;

namespace Tikker.EntityFramework.Services
{
    /// <summary>
    /// An entity represents something in the database.
    /// </summary>
    /// <typeparam name="T">an entity in the database</typeparam>
    public class GenericEntityService<T> : IEntityService<T> where T : Entity
    {
        protected readonly TikkerDbContextFactory _contextFactory;

        public GenericEntityService(TikkerDbContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task<T> Create(T entity)
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    EntityEntry<T> entry = await context.Set<T>().AddAsync(entity);
                    await context.SaveChangesAsync();

                    return entry.Entity;
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return null;
            }
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            try
            {

                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    ICollection<T> entities = await context.Set<T>().ToListAsync();
                    return entities;
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return new List<T>();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tikker.Domain.Models;
using Tikker.Domain.Services;

namespace Tikker.EntityFramework.Services
{
    public class BadgeService : GenericDataService<Badge>, IBadgeService
    {
        public BadgeService(TikkerDbContextFactory contextFactory) : base(contextFactory)
        {
        }

        /// <summary>
        /// Get all badges of a given category.
        /// </summary>
        /// <returns>A list of all badges that belong to the category</returns>
        public async Task<IEnumerable<Badge>> GetByCategory(BadgeCategory category)
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    return await context.Badges
                        .Where(badge => badge.BadgeCategory.Equals(category))
                        .Include(badge => badge.UserBadges)
                        .ToListAsync();
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return new List<Badge>();
            }
        }
        
        public new async Task<IEnumerable<Badge>> GetAll()
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    ICollection<Badge> entities = await context.Set<Badge>()
                        .Include(badge => badge.BadgeCategory)
                        .ToListAsync();
                    return entities;
                }
            }

            // in case no database connection is present
            catch (ArgumentException)
            {
                return new List<Badge>();
            }
        }
    }
}
﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tikker.Domain.Models;

namespace Tikker.EntityFramework.Services
{
    public class UserBadgesService : GenericEntityService<UserBadge>
    {
        public UserBadgesService(TikkerDbContextFactory contextFactory) : base(contextFactory)
        {
        }

        public async Task<int> GetAchievedForCategory(int categoryId)
        {
            try
            {
                using (TikkerDbContext context = _contextFactory.CreateDbContext())
                {
                    return await context.UserBadges
                        .Where(userBadge => userBadge.Badge.BadgeCategory.Id == categoryId)
                        .CountAsync();
                }
            }
            // in case no database connection is present
            catch (ArgumentException)
            {
                return 0;
            }
        }
    }
}
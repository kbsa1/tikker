﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tikker.EntityFramework.Migrations
{
    public partial class AddNewBadges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.InsertData(
                table: "Badges",
                columns: new[] { "Id", "Amount", "BadgeCategoryId", "Description" },
                values: new object[,]
                {
                    { 14, 2, 3, null },
                    { 15, 5, 3, null },
                    { 16, 1, 4, null },
                    { 17, 2, 4, null },
                    { 18, 3, 4, null },
                    { 19, 5, 6, null },
                    { 20, 10, 6, null },
                    { 21, 15, 6, null },
                    { 22, 20, 6, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Difficulty", "Text" },
                values: new object[] { 7, 400, "Now it's time for me to tell you about Young Nastyman, archrival and nemesis of Wonderboy, with powers comparable to Wonderboy. What powers you ask? I dunno how 'bout the power of flight? That do anything for ya? That's levitation, holmes" });
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tikker.EntityFramework.Migrations
{
    public partial class BadgeAndBadgeCategorySeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Badges_BadgeCategories_BadgeCategoryId",
                table: "Badges");

            migrationBuilder.AlterColumn<int>(
                name: "BadgeCategoryId",
                table: "Badges",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { "The amount of exercises you have completed in total", "/Views/Icons/check-bold.png", "Exercises completed" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { "The highest WPM value you have reached", "/Views/Icons/speedometer.png", "WPM reached" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { "The amount of times you have achieved 100% accuracy (in a row)", "/Views/Icons/target.png", "Times 100% accuracy achieved" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { "How many times you have beaten your own highscore", "/Views/Icons/trophy.png", "Highscores beaten" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { "The scores you have reached", "/Views/Icons/counter.png", "Score reached" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Description", "Name" },
                values: new object[] { "The amount of badges you have received in total", "Amount of badges received" });

            migrationBuilder.InsertData(
                table: "Badges",
                columns: new[] { "Id", "Amount", "BadgeCategoryId", "Description" },
                values: new object[,]
                {
                    { 13, 20, 5, null },
                    { 12, 10, 5, null },
                    { 11, 1000, 1, null },
                    { 10, 100, 1, null },
                    { 8, 10, 1, null },
                    { 7, 150, 2, "IMPOSSIBLE!!!" },
                    { 6, 120, 2, "INSANE!!!" },
                    { 5, 100, 2, "SONIC SPEED!" },
                    { 4, 85, 2, "LIGHTNING FAST!" },
                    { 3, 70, 2, "Better than the average professional typist!" },
                    { 2, 55, 2, "Pretty fast!" },
                    { 9, 50, 1, null },
                    { 1, 39, 2, "Above average!" }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Badges_BadgeCategories_BadgeCategoryId",
                table: "Badges",
                column: "BadgeCategoryId",
                principalTable: "BadgeCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Badges_BadgeCategories_BadgeCategoryId",
                table: "Badges");

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Badges",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.AlterColumn<int>(
                name: "BadgeCategoryId",
                table: "Badges",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { null, "/Views/Icons/medal.png", "Oefeningen voltooid" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { null, "/Views/Icons/medal.png", "WPM bereikt" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { null, "/Views/Icons/medal.png", "Nauwkeurigheid bereikt" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { null, "/Views/Icons/medal.png", "Highscores verslagen" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Description", "Icon", "Name" },
                values: new object[] { null, "/Views/Icons/medal.png", "Score behaald" });

            migrationBuilder.UpdateData(
                table: "BadgeCategories",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Description", "Name" },
                values: new object[] { null, "Aantal badges ontvangen" });

            migrationBuilder.AddForeignKey(
                name: "FK_Badges_BadgeCategories_BadgeCategoryId",
                table: "Badges",
                column: "BadgeCategoryId",
                principalTable: "BadgeCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tikker.EntityFramework.Migrations
{
    public partial class AddExercises : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Difficulty", "Text" },
                values: new object[,]
                {
                    { 1, 200, "Looking at the stars, looking at the sun. Light is still the same, when it comes to warm us. Sometimes there is no words, but silence is still talking. We look so different first, then we start to believe in something." },
                    { 22, 1400, "Setting sail once again they kept a sharp look-out for Busse Island, discovered thirty years previously by Martin Frobisher, but the rolling sea mists had grown too thick. Storms and gale—force winds plagued them for days on end and at one point grew so ferocious that the foremast cracked, splintered and was hurled into the sea." },
                    { 21, 1300, "But from this point on, the citizen-soldiers of Washington's army were no longer to be fighting only for the defense of their country, or for their rightful liberties as freeborn Englishmen, as they had at Lexington and Concord, Bunker Hill and through the long siege at Boston. It was now a proudly proclaimed, all-out war for an independent America, a new America, and thus a new day of freedom and equality." },
                    { 20, 1200, "We sing the freedom songs today for the same reason the slaves sang them, because we too are in bondage and the songs add hope to our determination that \"We shall over-come, Black and white together, We shall overcome some-day.\" I have stood in a meeting with hundreds of youngsters and joined in while they sang \"Ain't Gonna Let Nobody Turn Me \"Round.\" It is not just a song; it is a resolve." },
                    { 19, 1100, "Lydia was a stout, well-grown girl of fifteen, with a fine complexion and good-humoured countenance; a favou-rite with her mother, whose affection had brought her into public at an early age. She had high animal spirits, and a sort of natural self-consequence, which the atten-tions of the officers, to whom her uncle's good dinners and her own easy manners recommended her, had increased into assurance." },
                    { 18, 1000, "There may be less bacteria on the food that's picked up quickly, but playing it safe is the best idea. If it hits the floor, the next thing it should hit is the trash. If putting together petri dishes and dealing with incubation seems like a bigger project than you're ready to take on, there's a simpler way to observe bacterial growth. Practically all you need is some bread and your own two hands." },
                    { 17, 900, "Rube ran his ball club like it was a major league team. Most Negro teams back then weren't very well orga-nized. Didn't always have enough equipment or even matching uniforms. Most times they went from game to game scattered among different cars, or sometimes they'd even have to \"hobo\"—which means hitch a ride on the back of someone's truck to get to the next town for a game. But not Rube's team." },
                    { 16, 810, "Earth has been destroyed by war and no one lives on it anymore. The robots and the machines continue to function and serve human beings who have long ago died." },
                    { 15, 810, "Ruskin Bond used to spend his summer at his grandmother's house in Dehradun. While taking the train, he always had to pass through a small station called Deoli. No one used to get down at the station and nothing happened there. Until one day he sees a girl selling fruit and he is unable to forget her." },
                    { 14, 810, "According to all known laws of aviation, there is no way a bee should be able to fly. Its wings are too small to get its fat little body off the ground. The bee, of course, flies anyway because bees don't care what humans think is impossible." },
                    { 13, 800, "There wasn't much left in the tree fort from previous dwellers. Just an old hammer and a few rusted tin cans holding some even rustier nails. A couple of wood crates with the salt girl holding her umbrella painted on top. And a shabby plaque dangling sideways on one nail, FORT TREECONDEROGA. Probably named after the famous fort from Revolutionary War days." },
                    { 12, 700, "Edward, for lack of anything better to do, began to think. He thought about the stars. He remembered what they looked like from his bedroom window. What made them shine so brightly, he wondered, and were they still shining somewhere even though he could not see them? Never in my life, he thought, have I been farther away from the stars than I am now." },
                    { 11, 610, "A woman finds a pot of treasure on the road while she is returning from work. Delighted with her luck, she decides to keep it. As she is taking it home, it keeps changing. However, her enthusiasm refuses to fade away." },
                    { 10, 600, "\"We have to stop now,\" said Miss Lee. \"It's time for reading.\" \"Ohhh...\" A disappointed sound went up around the circle. \"Here's what we'll do.\" Miss Lee stood up. \"You are all very interested in dogs. So this week, you can write a story about your own dog or pet. Then you can read it to the class.\" Everyone got excited again. Except Posey. She didn't have a pet. Not a dog. Not a cat. Not a hamster." },
                    { 9, 600, "But from his first workout in Wood's Gymnasium he had been determined to control his asthma and illnesses rather than letting his asthma and illnesses control him. And he had. On that hot summer day in August he had proved to himself—and everyone else—that he had taken charge of his own life." },
                    { 8, 500, "Excuse me! Let's blow out of this place! In real life, germs are very small. They can't be seen without a microscope. Rudy forgot to use a tissue. His cold germs fly across the room at more than 100 miles an hour. Whee! I can fly! Best ride ever! A few germs land on Ernie. But skin acts like a suit of armor. It protects against harm. The germs won't find a new home there. Healthy skin keeps germs out." },
                    { 7, 400, "Now it's time for me to tell you about Young Nastyman, archrival and nemesis of Wonderboy, with powers comparable to Wonderboy. What powers you ask? I dunno how 'bout the power of flight? That do anything for ya? That's levitation, holmes" },
                    { 6, 400, "I continued to search. I checked under Steve's bed. Then I checked under my bed. I searched the basement, the garage, and my closet. There was no sign of Steve. This was going to be harder than I thought. Where was Steve hiding? CRASH! Uh-oh, I thought." },
                    { 5, 210, "Brian sat down for dinner. He sat down in the chair. He sat down at the table. He looked at his white plate. He looked at his silver fork. He looked at his silver spoon. His dad said, \"Pass me your plate, Brian. \"His dad put white rice on the plate. His dad put yellow corn on the plate. His dad put green peas on the plate. He gave the plate back to Brian. \"This looks delicious,\" Brian said. \"It is delicious,\" his dad said. Brian wondered why corn was yellow. He wondered why peas were green. He wondered if there were yellow peas and green corn." },
                    { 4, 210, "\"Can I ride my horse, Mommy?\" Sara asked her mom. Sara loved to ride her horse. She rode her horse almost every Saturday. \"Okay, honey, get ready to go,\" her mom said. Sara was happy. She went into her bedroom. She put her pink socks on. She put her pink sneakers on. She grabbed her pink hat. She went to the front door. \"I'm going to wait in the car,\" she told her mom. \"Okay, I'll be there in a minute,\" her mom said. Sara opened the car door. She sat down in the front seat. She put on her hat. She was excited." },
                    { 3, 210, "Girls like flowers, clever poetry. That old adage doesn't work on me, sorry. But conversation and a cup of tea? Boy, you had me at philosophy." },
                    { 2, 200, "He smacked the ball with the bat. The ball flew across the field. \"Good;' said Mr. Spano. \"Great, Slugger!\" I yelled. \"'We'll win every game. It was my turn next. I put on the helmet, and stood at home plate. \"Ronald Morgan,\" said Rosemary. \"You're holding the wrong end of the bat.\" Quickly I turned it around. I clutched it close to the end. Whoosh went the first ball. Whoosh went the second one. Wham went the third. It hit me in the knee." },
                    { 23, 1500, "The Words were to me so many Pearls of Eloquence, and his Voice sweeter to my Ears than Sugar to the Taste. The Reflection on the Misfortune which these Verses brought on me, has often made me applaud Plato's Design of ban-ishing all Poets from a good and well governed Common-wealth, especially those who write wantonly or lasciviously." },
                    { 24, 1875, "Gumarcaah, 362 as the Quiche named it when Kings Cotuha and Gucumatz and all the lords came. There had then begun the fifth generation of men, since the beginning of civilization and of the population, the beginning of the existence of the nation. There, then, they built many houses and at the same time constructed the temple of God; in the center of the high part of the town they located it when they arrived and settled there.‎" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 24);
        }
    }
}

﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tikker.Domain.Models;
using Tikker.Domain.Services;
using Tikker.WPF.Commands;
using Tikker.WPF.States.Navigators;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Tests.Commands
{
    [TestFixture]
    public class NextExerciseCommandTest
    {
        private Mock<IExerciseService> _exerciseService;
        private Mock<IStatisticsService> _statisticsService;
        private ExerciseViewModel _viewModel;
        private NextExerciseCommand _nextExerciseCommand;

        [SetUp]
        public void SetUp()
        {
            _exerciseService = new Mock<IExerciseService>();
            _statisticsService = new Mock<IStatisticsService>();
            _viewModel = new ExerciseViewModel(new Navigator());
            _nextExerciseCommand = new NextExerciseCommand(_viewModel, _exerciseService.Object, _statisticsService.Object);
        }

        [Test]
        public async Task ExecuteCommand_WithMultipleExercises_AllShouldOccur()
        {
            Exercise e1 = new Exercise() { Id = 1, Text = "1", Difficulty = 100 };
            Exercise e2 = new Exercise() { Id = 2, Text = "2", Difficulty = 100 };
            Exercise e3 = new Exercise() { Id = 3, Text = "3", Difficulty = 100 };
            _exerciseService.Setup(s => s.GetShuffledWithinDifficulty(0, 600)).ReturnsAsync(new List<Exercise>()
            {
                e1, e2, e3
            });
            _statisticsService.Setup(s => s.GetAverageScore()).ReturnsAsync(40);

            // check if all occur in any order
            List<string> occurred = new List<string>() { "1", "2", "3" };

            // starts as empty
            Assert.IsEmpty(_viewModel.Words);

            for(int i=0; i<3; ++i)
            {
                await Task.Run(() => _nextExerciseCommand.Execute(null));
                Assert.IsNotEmpty(_viewModel.Words);

                // remove from occurred, to ensure all occur
                string word = _viewModel.Words.First.Value;
                Assert.Contains(word, occurred);
                occurred.Remove(_viewModel.Words.First.Value);
            }
        }
    }
}

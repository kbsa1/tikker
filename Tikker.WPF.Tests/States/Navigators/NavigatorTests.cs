﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Tikker.WPF.States.Navigators;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Tests.States.Navigators
{
    [TestFixture]
    public class NavigatorTests
    {
        private INavigator _navigator { get; set; }

        [SetUp]
        public void SetUp()
        {
            _navigator = new Navigator();
        }

        /// <summary>
        /// When a new viewmodel is set, the previous one should be returned when Back() is called
        /// </summary>
        [Test]
        public void Back_WithPreviousViewModel_ReturnsPreviousViewModel()
        {
            // previous viewmodel that will be compared
            IViewModel toCompare = _navigator.CurrentViewModel;
            // switch current viewmodel to a different one
            _navigator.CurrentViewModel = new HomeViewModel(_navigator);
            // check if back is same
            IViewModel result = _navigator.Back();
            Assert.AreEqual(toCompare, result);
        }

        /// <summary>
        /// when there's multiple viewmodels in the history, the last one should be returned
        /// </summary>
        [Test]
        public void Back_WithMultiplePreviousViewModel_ReturnsLastVisitedViewModel()
        {
            IViewModel vm1 = new HomeViewModel(_navigator);
            IViewModel vm2 = new HomeViewModel(_navigator);
            IViewModel vm3 = new HomeViewModel(_navigator);
            _navigator.CurrentViewModel = vm1;
            _navigator.CurrentViewModel = vm2;
            _navigator.CurrentViewModel = vm3;
            // because vm3 is the current one, vm2 is the previous one
            IViewModel result = _navigator.Back();
            Assert.AreEqual(vm2, result);
        }

        /// <summary>
        /// When multiple viewmodels are in the history, and back is ran multiple times, it should still return the correct one.
        /// </summary>
        [Test]
        public void Back_RanTwice_ReturnsViewModelVisitedTwiceAgo()
        {
            IViewModel vm1 = new HomeViewModel(_navigator);
            IViewModel vm2 = new MainViewModel(_navigator);
            IViewModel vm3 = new ExerciseViewModel(_navigator);
            _navigator.CurrentViewModel = vm1;
            _navigator.CurrentViewModel = vm2;
            _navigator.CurrentViewModel = vm3;
            // because vm3 is the current one, vm1 will be the new current one when we go back twice
            _navigator.Back();
            IViewModel result = _navigator.Back();
            Assert.AreEqual(vm1, result);
        }

        /// <summary>
        /// when there's no viewmodel to go back to, null should be returned.
        /// </summary>
        [Test]
        public void Back_WithNoPreviousViewModels_ReturnsNull()
        {
            IViewModel result = _navigator.Back();
            Assert.AreEqual(null, result);
        }
    }
}

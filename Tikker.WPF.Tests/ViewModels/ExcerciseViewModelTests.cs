﻿using System.Collections.Generic;
using NUnit.Framework;
using Tikker.WPF.States.Navigators;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Tests.ViewModels
{
    [TestFixture]
    public class ExcersiseViewModelTests
    {
        private ExerciseViewModel _exerciseViewModel;

        [SetUp]
        public void SetUp()
        {
            _exerciseViewModel = new ExerciseViewModel(new Navigator());
        }

        [Test]
        public void Key_Char()
        {
            string expectedResult = "Key_C";
            string key = "c";
            string result = _exerciseViewModel.Key(key);
            Assert.AreEqual(expectedResult, result);
        }


        [Test]
        public void Key_Space()
        {
            string expectedResult = "Key_Space";
            string key = " ";
            string result = _exerciseViewModel.Key(key);
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ExerciseText_CurrentWord()
        {
            _exerciseViewModel.ExerciseText = "hallo allemaal";

            Assert.AreEqual("hallo ", _exerciseViewModel.CurrentWord.Value);
        }

        [Test]
        public void Live_WPM_Equals_10()
        {
            _exerciseViewModel.LiveWpm = 10;
            Assert.AreEqual(10, _exerciseViewModel.LiveWpm);
        }

        [Test]
        public void ExerciseInput_EqualsTestString()
        {
            _exerciseViewModel.CorrectlyTypedCharacters = 1;
            _exerciseViewModel.ExerciseText = "test";

            _exerciseViewModel.ExerciseInput = "test";

            Assert.AreEqual("test", _exerciseViewModel.ExerciseInput);
        }

        [Test]
        public void ExerciseInput_DoesNotEqualTeststring()
        {
            _exerciseViewModel.ExerciseText = "test";

            _exerciseViewModel.ExerciseInput = "tast";

            Assert.AreNotEqual("tost", _exerciseViewModel.ExerciseInput);
        }

        [Test]
        public void ExerciseInput_CheckInput_CurrentWordNull()
        {
            _exerciseViewModel.ExerciseInput = "a";
            _exerciseViewModel.ExerciseText = "a";

            Assert.AreEqual("a", _exerciseViewModel.ExerciseInput);
        }

        [Test]
        public void ExerciseInput_CheckInput_MultipleWords()
        {
            _exerciseViewModel.ExerciseText = "this is a test";

            _exerciseViewModel.ExerciseInput = "this ";

            Assert.AreEqual("", _exerciseViewModel.ExerciseInput);
        }

        [Test]
        public void KeyToType()
        {
            _exerciseViewModel.KeyToType = "Key_A";
            Assert.AreEqual("Key_A", _exerciseViewModel.KeyToType);
        }

        [Test]
        public void CaseSensitiveKey_GetSet()
        {
            _exerciseViewModel.CaseSensitiveKeyToType = "A";

            Assert.AreEqual("A", _exerciseViewModel.CaseSensitiveKeyToType);
        }
    }
}
﻿using System.Collections.Generic;
using System.Windows.Input;
using NUnit.Framework;
using Tikker.Domain.Models;
using Tikker.WPF.States.Navigators;
using Tikker.WPF.ViewModels;

namespace Tikker.WPF.Tests.ViewModels
{
    public class CategoriesViewModelTests
    {
        private CategoriesViewModel _categoriesViewModel;

        [SetUp]
        public void SetUp()
        {
            _categoriesViewModel = new CategoriesViewModel(new Navigator());
        }

        [Test]
        public void LoadCategoryCommand()
        {
            ICommand command = _categoriesViewModel.LoadCategoryCommand;
            BadgeCategory badgeCategory = new BadgeCategory();
            BadgesViewModel badgesViewModel = new BadgesViewModel(_categoriesViewModel.Navigator, badgeCategory);
            command.Execute(badgeCategory);
            
            Assert.AreEqual(badgesViewModel.GetType(), _categoriesViewModel.Navigator.CurrentViewModel.GetType());
        }

        [Test]
        public void TotalProgress_Update()
        {
            _categoriesViewModel.TotalBadges = 30;
            _categoriesViewModel.TotalBadgesAchieved = 21;
            Assert.AreEqual($"Total amount of badges achieved: {_categoriesViewModel.TotalBadgesAchieved} / {_categoriesViewModel.TotalBadges}", _categoriesViewModel.TotalProgress);
        }
    }
}
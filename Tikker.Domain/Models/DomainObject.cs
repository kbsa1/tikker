﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tikker.Domain.Models
{
    /// <summary>
    /// describes an entity in the database that is identifiable with an auto incremented field "Id"
    /// </summary>
    public abstract class DomainObject : Entity
    {
        /// <summary>
        /// unique identifier
        /// </summary>
        public int Id { get; set; }
    }
}

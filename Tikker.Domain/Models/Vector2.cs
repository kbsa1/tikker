﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tikker.Domain.Models
{
    /// <summary>
    /// a two-dimensional vector
    /// </summary>
    public class Vector2
    {
        /// <summary>
        /// get a vector2 with 0 values
        /// </summary>
        public static Vector2 Zero
        {
            get
            {
                return new Vector2(0, 0);
            }
        }

        /// <summary>
        /// get a vector2 with max values
        /// </summary>
        public static Vector2 Max
        {
            get
            {
                return new Vector2(Double.MaxValue, Double.MaxValue);
            }
        }

        /// <summary>
        /// the X position
        /// </summary>
        public double X { get; set; }
        /// <summary>
        /// the Y position
        /// </summary>
        public double Y { get; set; }

        /// <summary>
        /// constructor with doubles
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Vector2(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// constructor with ints. is converted to doubles.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}

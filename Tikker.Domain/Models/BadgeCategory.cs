﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tikker.Domain.Models
{
    /// <summary>
    /// a category of badges
    /// </summary>
    public class BadgeCategory : DomainObject
    {
        /// <summary>
        /// badges that belong to this category
        /// </summary>
        public virtual ICollection<Badge> Badges { get; set; }
        /// <summary>
        /// name of the category
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// a description of the badges in this category
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// name/path of icon to show for this category
        /// </summary>
        public string Icon { get; set; }
    }
}

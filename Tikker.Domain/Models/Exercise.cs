﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tikker.Domain.Models
{
    /// <summary>
    /// an exercise that can be typed
    /// </summary>
    public class Exercise : DomainObject
    {
        /// <summary>
        /// Text to be written
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// how difficult this text is.
        /// This is measured in Lexile Measure and is between 5 and 2000.
        /// </summary>
        public int Difficulty { get; set; }
        /// <summary>
        /// Users that added this Exercise. Unused because this feature has been removed.
        /// DEPRECATED
        /// </summary>
        public virtual ICollection<User> Users { get; set; }
    }
}

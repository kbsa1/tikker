﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tikker.Domain.Models
{
    /// <summary>
    /// Which user completed which badges.
    /// For the many-to-many relationship between users and badges.
    /// </summary>
    public class UserBadge : Entity
    {
        /// <summary>
        /// The id of the user that achieved the badge
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// The user that achieved the badge
        /// </summary>
        public virtual User User { get; set; }
        /// <summary>
        /// The id of the badge achieved by the user
        /// </summary>
        public int BadgeId { get; set; }
        /// <summary>
        /// The badge achieved by the user
        /// </summary>
        public virtual Badge Badge { get; set; }
        /// <summary>
        /// if the badge is completed
        /// DEPRECATED: Now the existance of an userbadge means the user achieved it
        /// </summary>
        public bool IsCompleted { get; set; }
    }
}

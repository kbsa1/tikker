﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection.PortableExecutable;
using System.Runtime.CompilerServices;
using System.Text;

namespace Tikker.Domain.Models
{
    /// <summary>
    /// result for an exercise
    /// </summary>
    public class Statistic : DomainObject
    {
        /*public User User { get; set; }*/
        /// <summary>
        /// Exercise that was completed
        /// </summary>
        public virtual Exercise Exercise { get; set; }
        /// <summary>
        /// Id of the exercise that aws completed
        /// </summary>
        public int ExerciseId { get; set;}
        /// <summary>
        /// typing speed measured in words per minute
        /// performed by the user.
        /// </summary>
        public int WPM { get; set; }
        /// <summary>
        /// how accurate the user typed the text in percentange.
        /// is always between 1 and 100
        /// </summary>
        public int Accuracy { get; set; }
        /// <summary>
        /// Score achieved by the user
        /// </summary>
        public int Score { get; set; }
        /// <summary>
        /// How long it took the user to complete
        /// </summary>
        public TimeSpan Time { get; set; }
        /// <summary>
        /// when the exercise was completed
        /// </summary>
        public DateTime TakenOn { get; set; }

        /// <summary>
        /// Get score based on current Statistic
        /// </summary>
        /// <returns></returns>
        public int CalculateScore()
        {
            return Statistic.CalculateScore(Exercise.Difficulty, Exercise.Text.Length, WPM, Accuracy);
        }

        /// <summary>
        /// calculate WPM based on type and text length
        /// </summary>
        /// <param name="minute">time it took to type written text</param>
        /// <param name="typedCharacters">amount of characters in written text</param>
        /// <returns></returns>
        public static int CalculateWPM(double minute, int typedCharacters)
        {
            double wpmdouble = ((double)typedCharacters / 5) / minute;
            int wpmInt = (int)wpmdouble;
            return wpmInt;
        }

        /// <summary>
        /// calculate the score
        /// </summary>
        /// <param name="difficulty">difficulty ranging from 5-2000</param>
        /// <param name="textLength">amount of characters in text</param>
        /// <param name="wpm">speed in words per minute</param>
        /// <param name="accuracy">accuracy in percentage (1-100)</param>
        /// <returns></returns>
        public static int CalculateScore(int difficulty, int textLength, int wpm, int accuracy)
        {
            float result = ((float)difficulty / 100 + (float)textLength / 10 + (float)wpm / 2)
                * (float)accuracy/100;
            return (int)result;
        }

        /// <summary>
        /// calculate accuracy
        /// </summary>
        /// <param name="wrongTypedCharacters">amount of characters incorrectly typed in text</param>
        /// <param name="totalCharacters">length of text</param>
        /// <returns></returns>
        public static int CalculateAccuracy(int wrongTypedCharacters, int totalCharacters)
        {
            int characters = totalCharacters - wrongTypedCharacters;
            float accuracyFloat = ((float)characters / (float)totalCharacters) * 100;
            int Accuracy = (int)accuracyFloat;
            return Accuracy;
        }
    }
}

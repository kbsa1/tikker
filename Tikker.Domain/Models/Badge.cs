﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tikker.Domain.Models
{
    /// <summary>
    /// A badge that can be obtained by an user
    /// </summary>
    public class Badge : DomainObject
    {
        /// <summary>
        /// Describes what must be achieved for this badge
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// the amount of times something must be achieved to receive this badge.
        /// For example a badge that you receive for completing 100 exercises would have this set as 100.
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// users who received this badge
        /// </summary>
        public virtual ICollection<UserBadge> UserBadges { get; set; }
        /// <summary>
        /// the category this badge belongs to
        /// </summary>
        public BadgeCategory BadgeCategory { get; set; }
        /// <summary>
        /// the id of the category this badge belongs to
        /// </summary>
        public int BadgeCategoryId { get; set; }
    }
}

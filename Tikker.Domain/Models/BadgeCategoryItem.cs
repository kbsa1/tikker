﻿namespace Tikker.Domain.Models
{
    /// <summary>
    /// A BadgeCategoryItem is used in the CategoriesViewModel,
    /// to show how many badges are achieved in a category.
    /// </summary>
    public class BadgeCategoryItem
    {
        /// <summary>
        /// a badge category
        /// </summary>
        public BadgeCategory BadgeCategory { get; set; }
        /// <summary>
        /// how many badges are achieved in the badge category
        /// </summary>
        public int AmountAchieved { get; set; }
    }
}
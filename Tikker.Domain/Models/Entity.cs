﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tikker.Domain.Models
{
    /// <summary>
    /// describes a class that is an entity in the database
    /// </summary>
    public abstract class Entity
    {
    }
}

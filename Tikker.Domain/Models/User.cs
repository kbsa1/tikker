﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tikker.Domain.Models
{
    /// <summary>
    /// Categories in difficulty for the user
    /// </summary>
    public enum DifficultyCategory
    {
        Beginner,
        Average,
        Experienced,
    }

    /// <summary>
    /// an student in the application that performs exercises
    /// </summary>
    public class User : DomainObject
    {
        /// <summary>
        /// Exercises added by the user, unused because its for a removed feature.
        /// DEPRECATED
        /// </summary>
        public virtual ICollection<Exercise> Exercises { get; set; }
        /// <summary>
        /// DEPRECATED
        /// </summary>
        public virtual ICollection<UserBadge> UserBadges { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tikker.Domain.Models;

namespace Tikker.Domain.Services
{

    /// <summary>
    /// data service for exercises,
    /// used for interactions with the database.
    /// </summary>
    public interface IExerciseService : IDataService<Exercise>
    {
        /// <summary>
        /// Get a list of randomly ordered exercises.
        /// </summary>
        /// <returns>A list of random exercises</returns>
        Task<IEnumerable<Exercise>> GetAllShuffled();

        /// <summary>
        /// Get a list of randomly ordered exercises within a specific difficulty.
        /// Higher or equal to min, and lower or equal to maxl.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns>A list of random exercises</returns>
        Task<IEnumerable<Exercise>> GetShuffledWithinDifficulty(int min, int max);
    }
}

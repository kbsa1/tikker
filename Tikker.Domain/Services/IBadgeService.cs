﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tikker.Domain.Models;

namespace Tikker.Domain.Services
{
    /// <summary>
    /// data service for badges,
    /// used for interactions with the database.
    /// </summary>
    public interface IBadgeService : IDataService<Badge>
    {
        /// <summary>
        /// get all badges with given badge category.
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        Task<IEnumerable<Badge>> GetByCategory(BadgeCategory category);
    }
}

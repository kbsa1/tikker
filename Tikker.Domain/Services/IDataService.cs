﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Tikker.Domain.Services
{
    /// <summary>
    /// Used to interact with the database for a given model.
    /// DataService takes a DomainObject (model/entity). This means the model has an id.
    /// This can then be used to perform extra commands on the database.
    /// </summary>
    public interface IDataService<T> : IEntityService<T>
    {
        /// <summary>
        /// get the given entity from the database using its id
        /// </summary>
        /// <param name="id">unique identifier</param>
        /// <returns></returns>
        Task<T> Get(int id);
        /// <summary>
        /// update the entity with a given id, with the data from the given entity
        /// </summary>
        /// <param name="id">unique identifier</param>
        /// <param name="entity">entity with new data</param>
        /// <returns></returns>
        Task<T> Update(int id, T entity);
        /// <summary>
        /// delete the entity with the given id from the database
        /// </summary>
        /// <param name="id">unique identifier</param>
        /// <returns></returns>
        Task<bool> Delete(int id);
    }
}

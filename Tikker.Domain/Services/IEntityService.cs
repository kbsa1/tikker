﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Tikker.Domain.Services
{
    /// <summary>
    /// data service used to interact with the database for a given entity (model).
    /// </summary>
    /// <typeparam name="T">Model class to interact with</typeparam>
    public interface IEntityService<T>
    {
        /// <summary>
        /// get all from the database
        /// </summary>
        /// <returns>a list of all entities in the database</returns>
        Task<IEnumerable<T>> GetAll();
        /// <summary>
        /// create given entity
        /// </summary>
        /// <param name="entity">entity to create</param>
        /// <returns></returns>
        Task<T> Create(T entity);
    }
}

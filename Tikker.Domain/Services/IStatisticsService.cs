﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tikker.Domain.Models;

namespace Tikker.Domain.Services
{
    /// <summary>
    /// data service for statistics,
    /// used for interactions with the database.
    /// </summary>
    public interface IStatisticsService : IDataService<Statistic>
    {
        /// <summary>
        /// Get the highscore for a given exercise
        /// </summary>
        /// <param name="statistic">the exercise to get the highscore for</param>
        /// <returns></returns>
        Task<Statistic> GetHighScore(int exerciseId);

        /// <summary>
        /// Get the highscore for a given exercise
        /// </summary>
        /// <param name="exerciseId">id of the exercise to get the highscore for</param>
        /// <returns></returns>
        Task<Statistic> GetHighScore(Exercise exercise);

        /// <summary>
        /// get the average score
        /// </summary>
        /// <returns>average score or -1 if no score</returns>
        Task<int> GetAverageScore();

        /// <summary>
        /// get difficulty category based on average score
        /// </summary>
        /// <returns>difficulty category</returns>
        Task<DifficultyCategory> GetDifficultyCategory();

        /// <summary>
        /// Get the current amount of 100% accuracy in a row
        /// </summary>
        /// <returns>the current 100% accuracy streak</returns>
        Task<int> GetAccuracyStreak();
    }
}

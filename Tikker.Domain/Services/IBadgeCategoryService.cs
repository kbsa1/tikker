﻿using System;
using System.Collections.Generic;
using System.Text;
using Tikker.Domain.Models;

namespace Tikker.Domain.Services
{
    /// <summary>
    /// data service for badge categories,
    /// used for interactions with the database.
    /// </summary>
    public interface IBadgeCategoryService : IDataService<BadgeCategory>
    {

    }
}
